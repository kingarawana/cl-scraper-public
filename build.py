import logging
import aws_lambda
import os
import subprocess
from pybuilder.core import init, use_plugin, depends, task
from pathlib import Path
from dotenv import load_dotenv

logger = logging.getLogger(__name__)

use_plugin("python.core")
use_plugin("python.install_dependencies")
use_plugin("python.unittest")
# use_plugin("python.coverage")
use_plugin('python.integrationtest')


default_task = "publish"

name = "scraper-scheduled"
version = "0.0.1"

@init
def initialize(project):
    project.build_depends_on_requirements("requirements.txt")
    project.build_depends_on('mockito')


@init(environments="dev")
def initialize_dev_env(project):
    env_path = Path('.') / 'dev.env'
    load_dotenv(dotenv_path=env_path)


@task("deploy", "Deploys scraper-scheduled to aws lambda")
@depends('clean', 'install_dependencies', 'run_unit_tests')
def deploy(project):
    logger.info('Deploying..')
    base_dir = os.getcwd()
    config_file = f'../../../config.yaml'
    build_dir = f'{project.get_property("basedir")}/{project.get_property("dir_source_main_python")}'
    os.chdir(build_dir)
    aws_lambda.deploy(build_dir, config_file=config_file, requirements=f'{base_dir}/requirements.txt')
    logger.info('.. deployment COMPLETE!')
    os.chdir(base_dir)


@task("build", "Builds scraper-scheduled to aws lambda")
@depends('clean', 'install_dependencies', 'run_unit_tests')
def build(project):
    logger.info('Building..')
    base_dir = os.getcwd()
    config_file = f'../../../config.yaml'
    build_dir = f'{project.get_property("basedir")}/{project.get_property("dir_source_main_python")}'
    os.chdir(build_dir)
    aws_lambda.build(build_dir, config_file=config_file, requirements=f'{base_dir}/requirements.txt')
    logger.info('.. build COMPLETE!')
    os.chdir(base_dir)


@task("push", "Push the latest build to s3")
def push_to_s3(project):
    search_dir = "dist/"
    files = os.listdir(search_dir)
    files.sort(key=lambda x: os.path.getmtime(f'{search_dir}{x}'), reverse=True)
    latest = f'{search_dir}{files[0]}'
    print('pushing: ', latest)
    cmd = f'aws s3 cp {latest} s3://real-estate-scraper/builds/cl-scraper/LATEST.zip'
    subprocess.call(cmd, shell=True)


@task("bp", "builds and push to s3")
@depends('build', 'push')
def build_and_push(project):
    pass

@task("dp", "deploys and push to s3")
@depends('deploy', 'push')
def deploy_and_push(project):
    pass