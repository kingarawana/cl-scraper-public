### Automated CL Scraper

pip install python-lambda
pip install pybuilder==0.12.0.dev20190116131423

pyb

pyb deploy


function createImage(){
	var posts = db.realestate.clposts.find();
	objs = []
	posts.forEach(function(post){
		post.images.forEach(function(i){
			objs.push({srcUrl: i, processingRequestTime: 1, finalUrl: null});
		});
	});
	db.realestate.images.insertMany(objs)
}

42,000 zips in the US (actually it's about 30k)

~327 in the LA

42000/327 = 128x
30000/327 = 100x

327 zips results in about = 149mb of data every 2 weeks

128 * 149 = 19072mb every 2 weeks = 19gb every two weeks
100 * 149 = 14900mb every 2 weeks = 15gb every two weeks

Pretend the rest of the US is half as dense as LA

So ~10gb every two weks -> 20gb per month -> 240gb per year -> 5years 1200gb = 1.2TB
So ~8gb every two weks -> 16gb per month -> 192gb per year -> 5years 960gb = 1TB


launch-script.sh
#!/usr/bin/bash
  
python3 /home/ec2-user/download-startup.py
sudo chmod +x /home/ec2-user/startup.sh
sh /home/ec2-user/startup.sh

download-startup.py
import boto3
resource = boto3.resource('s3')
scraper_bucket = resource.Bucket('real-estate-scraper')
scraper_bucket.download_file('builds/startup.sh', '/home/ec2-user/startup.sh')

download-cl-build.py
import boto3
resource = boto3.resource('s3')
scraper_bucket = resource.Bucket('real-estate-scraper')
scraper_bucket.download_file('builds/cl-scraper/LATEST.zip', '/home/ec2-user/LATEST.zip')