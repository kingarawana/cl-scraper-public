import unittest
import requests
import config
import api_service
import time
from mockito import when, any, unstub
API_URL = config.get('API_URL')
SUB_QUERY = '?sub=cl'


def check_url(url):
    if '2999' not in url:
        raise Exception('Should run as SCRAPER_ENV=test')


class TestBaseAssembler(unittest.TestCase):

    def setUp(self):
        if '2999' not in API_URL:
            raise Exception('Should run as SCRAPER_ENV=test')

    def tearDown(self):
        create_url = f'{API_URL}realestate/craigslist/zip/4kjh4hj23d' + SUB_QUERY
        requests.delete(create_url)
        create_url = f'{API_URL}realestate/craigslist/post/ij23jkheu3' + SUB_QUERY
        requests.delete(create_url)
        unstub()

    def test_get_next_zip(self):
        zip = {'zip': '90026', 'processingRequestTime': 1, 'lastCompletedProcess': 1}
        create_url = f'{API_URL}realestate/craigslist/zip'
        resp = requests.post(create_url + SUB_QUERY, json=zip)
        self.assertEqual(200, resp.status_code)
        zip_obj = api_service.get_next_zip()
        self.assertEqual(zip['zip'], zip_obj['zip'])
        self.assertIsNotNone(zip_obj['_id'])
        zip_id = zip_obj['_id']
        zip_obj = api_service.get_next_zip()
        self.assertIsNone(zip_obj)

        resp = requests.get(create_url + '/' + zip_id + SUB_QUERY)
        zip_resp = resp.json()['data']
        self.assertEqual(zip['lastCompletedProcess'], zip_resp['lastCompletedProcess'])
        self.assertGreater(zip_resp['processingRequestTime'], zip['processingRequestTime'])

    def test_mark_zip_complete(self):
        now = time.time() * 1000
        zip = {'zip': '90026', 'processingRequestTime': 1, 'lastCompletedProcess': 1}
        create_url = f'{API_URL}realestate/craigslist/zip'
        resp = requests.post(create_url + SUB_QUERY, json=zip)
        self.assertEqual(200, resp.status_code)
        zip_obj = api_service.get_next_zip()
        self.assertEqual(zip['zip'], zip_obj['zip'])
        self.assertIsNotNone(zip_obj['_id'])
        zip_id = zip_obj['_id']
        zip_obj = api_service.get_next_zip()
        self.assertIsNone(zip_obj)

        resp = requests.get(create_url + '/' + zip_id + SUB_QUERY)
        zip_resp = resp.json()['data']
        self.assertEqual(zip['lastCompletedProcess'], zip_resp['lastCompletedProcess'])
        self.assertGreater(zip_resp['processingRequestTime'], now)

        api_service.mark_zip_complete(zip_resp['_id'])

        resp = requests.get(create_url + '/' + zip_id + SUB_QUERY)
        zip_resp = resp.json()['data']

        self.assertGreater(zip_resp['lastCompletedProcess'], now)
        self.assertGreater(zip_resp['processingRequestTime'], now)

    def test_create_posts(self):
        zip = '90026'
        post1 = {
            'postId': '111',
            'postUrl': "htttp://abc.com/1",
            'zip': zip,
            'price': 1000
        }
        post2 = {
            'postId': '222',
            'postUrl': "htttp://abc.com/2",
            'zip': zip,
            'price': 2000
        }

        api_service.create_posts([post1, post2])

        create_url = f'{API_URL}realestate/craigslist/post' + SUB_QUERY
        resp = requests.get(create_url)
        data = resp.json()['data']
        self.assertEqual(2, len(data))

    def test_get_needs_scrape(self):
        zip = '90026'
        post1 = {
            'postId': '111',
            'postUrl': "htttp://abc.com/1",
            'zip': zip,
            'price': 1000
        }
        post2 = {
            'postId': '222',
            'postUrl': "htttp://abc.com/2",
            'zip': zip,
            'price': 2000
        }
        post3 = {
            'postId': '333',
            'postUrl': "htttp://abc.com/3",
            'zip': zip,
            'price': 3000
        }

        api_service.create_posts([post1, post2, post3])

        create_url = f'{API_URL}realestate/craigslist/post' + SUB_QUERY
        resp = requests.get(create_url)
        data = resp.json()['data']
        self.assertEqual(3, len(data))

        ids_prices = [
            {'postId': '111', 'price': 1000},
            {'postId': '333', 'price': 3001},
            {'postId': '444', 'price': 4000}
        ]

        data = api_service.get_needs_scrape(ids_prices)

        self.assertEqual(2, len(data))
        self.assertTrue('333' in data)
        self.assertTrue('444' in data)

    def test_get_urls_to_check(self):
        zip = '90026'
        post1 = {
            'postId': '111',
            'postUrl': "htttp://abc.com/1",
            'zip': zip,
            'price': 1000
        }
        post2 = {
            'postId': '222',
            'postUrl': "htttp://abc.com/2",
            'zip': zip,
            'price': 2000
        }
        post3 = {
            'postId': '333',
            'postUrl': "htttp://abc.com/3",
            'zip': zip,
            'price': 3000
        }
        post4 = {
            'postId': '444',
            'postUrl': "htttp://abc.com/4",
            'zip': zip,
            'price': 4000
        }

        api_service.create_posts([post1, post2, post3, post4])

        create_url = f'{API_URL}realestate/craigslist/post' + SUB_QUERY
        resp = requests.get(create_url)
        data = resp.json()['data']
        self.assertEqual(4, len(data))

        urls = [
            post1['postUrl'],
            post3['postUrl'],
            "htttp://abc.com/5",
            "htttp://abc.com/6"
        ]

        query = {'zip': zip}

        data = api_service.get_urls_to_check(query, urls)
        self.assertEqual(2, len(data))
        self.assertTrue(post2['postUrl'] in data)
        self.assertTrue(post4['postUrl'] in data)

    def test_update_posts(self):
        zip = '90026'
        post1 = {
            'postId': '111',
            'postUrl': "htttp://abc.com/1",
            'zip': zip,
            'price': 1000
        }
        post2 = {
            'postId': '222',
            'postUrl': "htttp://abc.com/2",
            'zip': zip,
            'price': 2000
        }

        api_service.create_posts([post1, post2])

        create_url = f'{API_URL}realestate/craigslist/post' + SUB_QUERY
        resp = requests.get(create_url)
        data = resp.json()['data']
        self.assertEqual(2, len(data))
        for d in data:
            self.assertFalse('expiredTime' in d)
            self.assertFalse('removedTime' in d)

        update = {
            'postId': '222',
            'expiredTime': 1111
        }
        api_service.update_posts([update])
        resp = requests.get(create_url)
        data = resp.json()['data']
        for d in data:
            if d['postId'] == '222':
                self.assertEqual(d['expiredTime'], update['expiredTime'])
                self.assertFalse('removedTime' in d)
            else:
                self.assertFalse('expiredTime' in d)
                self.assertFalse('removedTime' in d)


if __name__ == '__main__':
    unittest.main()