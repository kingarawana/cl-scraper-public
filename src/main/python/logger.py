import logging
import json
import config

if config.is_prod():
    logging.basicConfig(level=logging.ERROR)
    logging.basicConfig(level=logging.ERROR)
    _logger = logging.getLogger('SCRAPER-APP')
    _logger.setLevel(logging.ERROR)
else:
    _logger = logging.getLogger('SCRAPER-APP')
    _logger.setLevel(logging.INFO)

# _logger.setLevel(logging.INFO)

def error(msg, *args, **kwargs):
    _logger.error(f'APPLICATION ERROR: {msg}: data: {_create_obj(args)}')


def warning(msg, *args, **kwargs):
    _logger.warning(f'APPLICATION WARNING: {msg}: data: {_create_obj(args)}')


def info(msg, *args):
    _logger.info(f'APPLICATION INFO: {msg}: data: {_create_obj(args)}')


def debug(msg, *args, **kwargs):
    _logger.debug(f'APPLICATION DEBUG: {msg}: data: {_create_obj(args)}')


def _create_obj(args):
    s = ''
    for arg in args:
        if isinstance(arg, Exception):
            s = s + repr(arg)
        else:
            s = s + json.dumps(arg)
    return s


if __name__ == '__main__':
    info('xxxxx', {'data': 'fff'}, {'data': 'eeee'})