import requests
import config
import logger
import json
import sleeper

SUB_QUERY = '?sub=cl'
GET_ZIP_PATH = 'realestate/craigslist/zip/next' + SUB_QUERY
POST_PATH = 'realestate/craigslist/post' + SUB_QUERY
SHOULD_SCRAPE_PATH = 'realestate/craigslist/post/scrape' + SUB_QUERY
MARK_ZIP_COMPLETE_PATH = 'realestate/craigslist/zip/{}/complete' + SUB_QUERY
CHECK_ACTIVE_PATH = 'realestate/craigslist/active' + SUB_QUERY
API_URL = config.get('API_URL')

MAX_RETRY = 4

def get_next_zip():
    logger.debug('Getting next zip')
    """
    Gets the next zip to process
    :return: Number
    """
    resp = requests.get(API_URL + GET_ZIP_PATH)
    return _handle_resp(resp, 'get_next_zip')


def get_needs_scrape(ids_prices):
    """
    :param body: [{postId: "123", price: 1900}, {...}]
    :return: [postId1, postId2, ..]
    """
    resp = requests.post(API_URL + SHOULD_SCRAPE_PATH, json=ids_prices)
    return _handle_resp(resp, 'get_needs_scrape')


def create_posts(posts):
    if not posts:
        return
    logger.debug(f'Creating posts from posts. Number of posts: {len(posts)}')
    """
    For creating a post in db.
    :param post: {}
    :return:
    """
    resp = requests.post(API_URL + POST_PATH, json=posts)
    if resp.status_code == 200:
        return resp.json()['data']
    elif resp.status_code == 400:
        logger.error(f'Post already exists')
    else:
        raise Exception('HTTP Error: ' + str(resp.status_code), resp.json())


def update_posts(posts):
    """
    For when find out why a post expired. We call this to
    update the appropriate reason field w/ the timestamp.
    :param post: object w/ fields to update.
    :return:
    """
    # resp = requests.put(API_URL + POST_PATH, post)
    # return _handle_resp(resp, 'update_post')
    resp = requests.put(API_URL + POST_PATH, json=posts)
    return _handle_resp(resp, 'update_posts')


def mark_zip_complete(zip_id):
    logger.debug(f'Marking zip complete: {zip}')
    """
    Marks the zip as having been processed successfully.
    :param zip_id:
    :return:
    """
    resp = requests.put(API_URL + MARK_ZIP_COMPLETE_PATH.format(zip_id))
    return _handle_resp(resp, 'mark_zip_complete')


def get_urls_to_check(query, post_urls):
    """
    Returns all urls that are in the DB, but not included in the `post_urls`.
    :param urls: [url1, url2, ..]
    :return: [url1, url2, ..]
    """
    data = {
        'query': query,
        'postUrls': post_urls
    }
    resp = requests.post(API_URL + CHECK_ACTIVE_PATH, json=data)
    return _handle_resp(resp, 'get_urls_to_check')


def _handle_resp(resp, fn_name=''):
    retries = 0
    while retries <= MAX_RETRY:
        try:
            if resp.status_code == 200:
                return resp.json()['data']
            else:
                raise Exception('HTTP Error: ' + str(resp.status_code), fn_name, resp)
        except Exception as e:
            if retries > MAX_RETRY:
                raise e
            retries = retries + 1
            sleeper.sleep(1,2)


if __name__ == '__main__':
    pass