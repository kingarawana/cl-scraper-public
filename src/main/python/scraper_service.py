import dateutil.parser
import re
import image_service
from datetime import datetime
from enum import Enum
import logger

regex = re.compile('[a-zA-Z$]')


class PageStatusEnum(Enum):
    EXPIRED = 0
    ACTIVE = 1
    REMOVED = -1
    MISSING = -2


def get_post_url_from_post(element):
    el = element.find('a', class_='result-image gallery')
    if not el:
        el = element.find('a', class_='result-image gallery empty')
    return el.attrs['href']


def get_post_id_from_post_url(post_url):
    return post_url.split('/')[-1].split('.')[0]


def get_posts_from_search_page(search_page):
    return search_page.find_all('li', class_='result-row')


def get_price_from_post(post):
    price = post.find('span', class_='result-price').text
    if not price:
        price = post.find('p', class_='result-info').find('span', class_='result-price').text.trim()
    if price:
        price = int(price.replace("$", ""))
    else:
        price = None
    return price


def get_price_from_page(page):
    el = page.find('span', class_='price')
    if not el:
        return -1
    return int(regex.sub('', el.text))

def get_images(page):
    images = page.find_all('a', class_='thumb')
    return [img.attrs['href'] for img in images]


def get_description(page):
    body = page.find('section', id='postingbody')
    return body.text


def get_title(page):
    el = page.find('span', class_='postingtitletext')
    return el.text


def get_tags(page):
    attrs = page.find_all('p', class_='attrgroup')
    results = []
    num_attrs = len(attrs)
    # if(num_attrs > 1):
    #     for i in range(1, num_attrs):
    #         sub_attrs = attrs[i].find_all('span')
    #         results.extend([attr.text.strip() for attr in sub_attrs])
    # else:
    #     logger.info('get_tags returned no tags *******')
    for i in range(0, num_attrs):
        sub_attrs = attrs[i].find_all('span')
        results.extend([attr.text.strip() for attr in sub_attrs])
    return results


def get_parking_type_from_tags(tags):
    if 'carport' in tags:
        return 'CARPORT'
    elif 'street parking' in tags:
        return 'STREET_PARKING'
    elif 'no parking' in tags:
        return 'NO_PARKING'
    elif 'off-street parking' in tags:
        return 'DRIVEWAY_PARKING'
    elif 'detached garage' in tags:
        return 'DETACHED_GARAGE'
    elif 'attached garage' in tags:
        return 'ATTACHED_GARAGE'
    else:
        logger.info("PARKING TYPE NOT FOUND: ", tags)
        return ''


def get_details(page):
    def get_num_bedrooms(text):
        data = text.split('/')
        try:
            return int(regex.sub('', data[0]))
        except:
            return -1

    def get_num_baths(text):
        data = text.split('/')
        if len(data) > 0:
            try:
                baths = float(regex.sub('', data[1]))
            except:
                baths = -1
        return baths

    def get_date_available(span):
        date = datetime.strptime(span.attrs['data-date'], '%Y-%m-%d')
        if date:
            return date.timestamp()
        else:
            logger.info("get_date_available DID NOT CONTAIN DATE VALUE *******")

    def get_sqft(text):
        idx = text.find('ft')
        try:
            if idx > -1:
                return int(text[:idx])
            else:
                return int(text)
        except:
            return None

    attrs_els = page.find_all('p', class_='attrgroup')
    results = {}
    if (len(attrs_els) > 0):
        spans = attrs_els[0].find_all('span')
        attrs = [attr.text for attr in spans]
        for idx, attr in enumerate(attrs):
            if 'ft' in attr:
                results['sqft'] = get_sqft(attr)
            elif ' / ' in attr:
                results['numBeedrooms'] = get_num_bedrooms(attr)
                results['numBaths'] = get_num_baths(attr)
            elif 'available' in attr:
                results['dateAvailable'] = get_date_available(spans[idx])
    else:
        logger.info('get_tags RETURNED NO ITEMS *******')

    return results


def get_address(page):
    el = page.find('div', class_='mapaddress')
    if (el):
        result = el.text
        idx = result.find(' near')
        if idx > -1:
            result = result[:idx]
        return result


def get_lat_long(page):
    el = page.find('p', class_='mapaddress')
    if (el):
        a = el.find('a')
        if (a):
            url = a.attrs['href']
            idx = url.find('@')
            lat_long = url[idx + 1:].split(',')
            if (len(lat_long) > 2):
                lat_long = lat_long[0:2]
            return lat_long


def get_page_status(page):
    """
        1 = active, 0 = expired, -1 = removed
    """
    removed = page.find_all('div', class_='removed')
    missing = page.find('h1', class_='post-not-found-heading')
    if not removed and not missing:
        return PageStatusEnum.ACTIVE

    if removed and 'expired' in removed[0].text:
        return PageStatusEnum.EXPIRED
    if missing:
        return PageStatusEnum.MISSING
    return PageStatusEnum.REMOVED


def get_post_date_in_seconds(page):
    el = page.find('time', class_='date timeago')
    dt = dateutil.parser.parse(el.attrs['datetime'])
    return int(dt.timestamp())


def get_post_data_post_page(post_page, url, zip, price):
    tags = get_tags(post_page)
    details = get_details(post_page)
    post_id = get_post_id_from_post_url(url)
    return {
        'postId': post_id,
        'postUrl': url,
        'title': get_title(post_page),
        'description': get_description(post_page),
        'street': get_address(post_page),
        'zip': zip,
        'latLong': get_lat_long(post_page),
        'price': price or get_price_from_page(post_page),
        'numBedrooms': details.get('numBeedrooms'),
        'numBaths': details.get('numBaths'),
        'sqft': details.get('sqft'),
        'parkingType': get_parking_type_from_tags(tags),
        'dateAvailable': details.get('dateAvailable'),
        'postDate': get_post_date_in_seconds(post_page),
        'images': image_service.save_cl_images_from_urls(get_images(post_page), post_id),
        'tags': tags,
        # 'hasParking': get_description(post_page),
        # 'numParking': '',

    }