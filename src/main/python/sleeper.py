from time import sleep as sl
from random import randint, uniform


def sleep(min_time=1, max_time=4):
    sl(randint(min_time, max_time))


def sleep_ms(min_time=0.1, max_time=0.5):
    sl(uniform(min_time, max_time))