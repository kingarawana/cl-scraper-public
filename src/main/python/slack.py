import requests
ALERT_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
STATUS_UPDATES_WEBHOOK = 'https://hooks.slack.com/services/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'


def send_text_alert(text):
    message = {'text': text}
    requests.post(ALERT_WEBHOOK_URL, json=message)


def send_status_update(text):
    message = {'text': text}
    requests.post(STATUS_UPDATES_WEBHOOK, json=message)


if __name__ == '__main__':
    obj = {'hello': 'world'}
    send_status_update(f'Status Update! 2 {obj}')
