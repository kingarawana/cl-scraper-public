import time
import s3
import logger

class ProcessLock:
    """
    Ensure that only a single process is run at a time. It uses a file in s3 to maintain the lock.
    """
    def __init__(self, lock_file_key, threshold_buffer_in_seconds=300):
        self.lock_key = lock_file_key
        self.threshold_buffer_in_seconds = threshold_buffer_in_seconds

    def _delete_old_locks(self, lock):
        threshhold = time.time() - self.threshold_buffer_in_seconds
        if lock['timestamp'] < threshhold:
            logger.info('expiring old lock')
            s3.delete(self.lock_key)

    def run_process_if_not_locked(self, request_id, process_to_run, *args):
        if not self.lock_key:
            raise Exception('Please initialize ProcessLock with unique'
                            ' desired lock file path. e.g. "locks/{process-name}"')
        lock = s3.get_json_object_by_key(self.lock_key)

        new_lock = {
            'request_id': request_id,
            'timestamp': time.time()
        }

        if not lock:
            lock = new_lock
            s3.put_json_object_by_key(self.lock_key, new_lock)
        elif lock['request_id'] == request_id:
            lock = new_lock
            s3.put_json_object_by_key(self.lock_key, new_lock)
        else:
            self._delete_old_locks(lock)

        if lock['request_id'] == request_id:
            logger.info('acquired lock')
            process_to_run(*args)
            s3.delete(self.lock_key)