import boto3
import logger
from boto3.dynamodb.conditions import Key, Attr


CLIENT = None
ACTIVE_POSTS_TABLE = None
CL_ACTIVE_POSTS_TABLE_NAME = 'CL_ActivePostsTable'

def get_client():
    global CLIENT
    if not CLIENT:
        CLIENT = boto3.client('dynamodb')
    return CLIENT


def get_active_table():
    global ACTIVE_POSTS_TABLE
    if not ACTIVE_POSTS_TABLE:
        ACTIVE_POSTS_TABLE = get_client().Table(CL_ACTIVE_POSTS_TABLE_NAME)
    return ACTIVE_POSTS_TABLE

def get_needs_scrape(ids_prices):
    """
    :param body: [{postId: "123", price: 1900}, {...}]
    :return: [postId1, postId2, ..]
    """
    post_ids = map(lambda i: i['postId'], ids_prices)

    table = get_active_table()
    response = table.query(
        KeyConditionExpression=Key('year')
    )


def get_urls_to_check(query, post_urls):
    """
    Returns all urls that are in the DB, but not included in the `post_urls`.
    :param urls: [url1, url2, ..]
    :return: [url1, url2, ..]
    """
    table = get_active_table()


def create_posts(posts):
    if not posts:
        return
    logger.debug(f'Creating posts from posts. Number of posts: {len(posts)}')
    """
    For creating a post in db.
    :param post: {}
    :return:
    """
    table = get_active_table()


def update_posts(posts):
    """
    For when find out why a post expired. We call this to
    update the appropriate reason field w/ the timestamp.
    :param post: object w/ fields to update.
    :return:
    """
    # resp = requests.put(API_URL + POST_PATH, post)
    # return _handle_resp(resp, 'update_post')
    table = get_active_table()

