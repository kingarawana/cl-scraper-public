import datetime
import json
import logging
import boto3
import requests
import io
from dateutil.tz import tzutc

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)

S3_CLIENT = None
S3_VERSIONED_CLIENT = None
APT_VERSIONED_BUCKET_NAME = None

SCRAPE_BUCKET_NAME = 'real-estate-scraper'


def get_s3_client():
    global S3_CLIENT
    if not S3_CLIENT:
        try:
            session = boto3.Session(profile_name='default')
            S3_CLIENT = session.client('s3')
        except:
            S3_CLIENT = boto3.client('s3')

    return S3_CLIENT


def put_json_object_by_key(key, json_object):
    json_string = json.dumps(json_object)
    get_s3_client().put_object(Body=json_string, Bucket=SCRAPE_BUCKET_NAME, Key=key)


def get_json_object_by_key(key):
    try:
        raw_object = get_raw_object(key)
        return json.loads(raw_object)
    except get_s3_client().exceptions.NoSuchKey as e:
        logger.warning("no such key: {}".format(key), exc_info=False)
        return None
    except Exception as e:
        logger.warning("failed to get_json_object_by_key for key: {}".format(key), exc_info=True)
        raise

def delete(key):
    get_s3_client().delete_object(Bucket=SCRAPE_BUCKET_NAME, Key=key)


def list_objects(prefix, max_keys=1000, marker=''):
    try:

        raw_object = get_s3_client().list_objects(Bucket=SCRAPE_BUCKET_NAME, Delimiter='/', Prefix=prefix,
                                                  MaxKeys=max_keys, Marker=marker)

        return {'Prefixes': raw_object.get('CommonPrefixes'),
                'Contents': raw_object.get('Contents'),
                'IsTruncated': raw_object.get('IsTruncated')}
    except Exception as e:
        logger.warning("failed to list_objects for prefix: {}".format(prefix), exc_info=True)
        return {}


def get_key_age(key):
    last_modified_time = get_s3_client().get_object(Bucket=SCRAPE_BUCKET_NAME, Key=key)['LastModified']
    current_time = datetime.datetime.now(tzutc())
    return current_time - last_modified_time


def get_object_by_key(key):
    try:
        client = get_s3_client()
        return client.get_object(Bucket=SCRAPE_BUCKET_NAME, Key=key)
    except Exception as e:
        logger.warning("failed to get_object_by_key for prefix: {}".format(key), exc_info=True)
        return None


def get_raw_object(key):
    return get_s3_client().get_object(Bucket=SCRAPE_BUCKET_NAME, Key=key)['Body'].read()


def copy_image_to_bucket(srcUrl, key):
    data = requests.get(srcUrl).content
    print(len(data), SCRAPE_BUCKET_NAME, key)
    get_s3_client().upload_fileobj(io.BytesIO(data), SCRAPE_BUCKET_NAME, key)


if __name__ == '__main__':
    url = 'https://images.craigslist.org/00101_g7R9pbk0rEV_600x450.jpg'
    data = requests.get(url).content
    get_s3_client().upload_fileobj(io.BytesIO(data), SCRAPE_BUCKET_NAME, 'images/cl/11111/00101_g7R9pbk0rEV_600x450.jpg')