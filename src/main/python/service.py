# -*- coding: utf-8 -*-
import api_service
import zip_processor
import logger
import slack
import sleeper
import traceback
from exceptions import ApiException

MAX_RETRY = 7


def handler(event, context):
    logger.info('Started zip process')
    zip_obj = api_service.get_next_zip()
    logger.info('Starting process for zip: ', zip_obj)
    retries = 0
    while zip_obj:
        try:
            print('starting new zip: ', zip_obj)
            zip_processor.process_zip(zip_obj['zip'], zip_obj['clSubDomain'])
            logger.info('Zip process complete: ', zip_obj)
            api_service.mark_zip_complete(zip_obj['_id'])
            logger.info('Marked zip: ', zip_obj)
            zip_obj = api_service.get_next_zip()
            logger.info('Got next zip: ', zip_obj)
            retries = 0
        except ApiException as e:
            if retries < MAX_RETRY:
                sleeper.sleep(1, 2)
                logger.error(f'ApiException, retrying', zip_obj, e)
                retries = retries + 1
            else:
                logger.error(f'ApiException MAX RETRIES EXCEEDED. Stopping', zip_obj, e)
                zip_obj = None
        except ConnectionError as e:
            if retries > MAX_RETRY:
                slack.send_text_alert(f'Max retries reached. ConnectionError. {zip_obj}. Err: {e}')
                logger.error(f'Max retries reached. ConnectionError', zip_obj, e)
                zip_obj = None
            else:
                slack.send_text_alert(f'Network down error. Sleeping then restarting. {zip_obj}. Err: {e}')
                logger.error(f'Network down error. Sleeping then restarting', zip_obj, e)
                sleeper.sleep(6, 7)
                retries = retries + 1
        except Exception as e:
            print(e)
            err_msg = repr(e).lower()
            if 'closed connection without' in err_msg:
                if retries < MAX_RETRY:
                    sleeper.sleep(6, 7)
                    logger.error(f'Processing failed, retrying', zip_obj, e)
                    retries = retries + 1
                else:
                    logger.error(f'MAX RETRIES EXCEEDED. Trying next zip', zip_obj, e)
                    zip_obj = api_service.get_next_zip()
            elif 'jsondecodeerror' in err_msg:
                if retries < MAX_RETRY:
                    sleeper.sleep(2, 4)
                    logger.error(f'Processing failed, retrying', zip_obj, e, err_msg)
                    retries = retries + 1
                else:
                    logger.error(f'MAX RETRIES EXCEEDED jsondecodeerror. Trying next zip', zip_obj, e)
                    zip_obj = api_service.get_next_zip()
            else:
                logger.error(f'FAILED PROCESSING', zip_obj, e, err_msg, traceback.format_exc())
                slack.send_text_alert(f'FAILED PROCESSING {zip_obj}. Going to next zip. Err: {e} err_msg: {err_msg} traceback: {traceback.format_exc()}')
                # zip_obj = None
                # raise e
                zip_obj = api_service.get_next_zip()

    logger.info('Process Complete: No more zips to process: ')


if __name__ == '__main__':
    handler(None, None)
    # zip_obj = {'_id': '5da66f269a4feb12bfeccd43', 'zip': '73142', 'clSubDomain': 'oklahomacity'}
    # zip_processor.process_zip(zip_obj['zip'], zip_obj['clSubDomain'])
    # api_service.mark_zip_complete(zip_obj['_id'])