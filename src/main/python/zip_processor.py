import requests
import scraper_service
import api_service
import time
import logger
import sys
import slack
from requests import get
from bs4 import BeautifulSoup
from scraper_service import PageStatusEnum
import helper
import sleeper
MAX_RETRY = 5



def get_zip_http_query(zip, cl_sub_domain):
    return f'https://{cl_sub_domain}.craigslist.org/search/apa?postal={zip}&availabilityMode=0&sale_date=all+dates'


def get_page_from_url(url):
    retries = 0
    while retries <= MAX_RETRY:
        try:
            resp = get(url)
            if resp.status_code == 200 or resp.status_code == 404:
                if len(resp.text) < 200 and 'blocked' in resp.text:
                    slack.send_text_alert(f'Got a blocked reponse for url: {url}')
                    return None
                return BeautifulSoup(resp.text, 'html.parser')
            else:
                raise Exception(f'Error get_page_from_url: url: {url} {str(resp.status_code)}', resp)
        except Exception as e:
            if retries > MAX_RETRY:
                raise e
            retries = retries + 1
            sleeper.sleep(1,2)


def process_zip(zip, cl_sub_domain):
    logger.info(f'Started processing zip: {zip}')
    search = get_zip_http_query(zip, cl_sub_domain)
    logger.info(f'Processing query URL: {search}')
    query_page = get_page_from_url(search)
    if not query_page:
        logger.warning('IP was blocked')
        slack.send_status_update(f'IP was blocked. URL: {search}')
        return
    results_num = query_page.find('div', class_='search-legend')
    if not results_num:
        logger.info(f'No search legend for zip: {zip}')
        return
    count_el = results_num.find('span', class_='totalcount')
    if not count_el:
        logger.info(f'No totalCount for zip: {zip}')
        return
    results_total = int(count_el.text)

    pages = helper.np_arrange(0, results_total + 1, 120)

    id_prices = []
    id_urls = {}

    for idx, page in enumerate(pages):
        # sleeper.sleep_ms()

        if idx != 0:
            query_url = search + "&s=" + str(page)
            logger.info(f'Processing query URL: {query_url}')
            response = get(query_url)
            search_page = BeautifulSoup(response.text, 'html.parser')
            if response.status_code != 200:
                logger.error('Request: {}; Status code: {}'.format(requests, response.status_code))
                raise Exception(f'Error getting the page for zip: {zip}. idx: {idx}, page: {page}')
        else:
            search_page = query_page

        posts = scraper_service.get_posts_from_search_page(search_page)

        for post in posts:
            post_url = scraper_service.get_post_url_from_post(post)
            post_id = scraper_service.get_post_id_from_post_url(post_url)
            price = scraper_service.get_price_from_post(post)
            id_urls[post_id] = post_url
            # there is a potential for a bug here since price could come back None
            id_prices.append({'postId': post_id, 'price': price})

        logger.info(f'Page {str(idx)} scraped successfully!')
        print(f'Page {str(idx)} scraped successfully!')

    process_posts(id_prices, id_urls, zip)
    process_urls({'zip': zip}, list(id_urls.values()))

    logger.info(f'Scrape complete zip: {zip}')


def process_posts(ids_prices, post_id_urls, zip):
    """
    Handles the creation of new posts in the system.
    Or duplication of a post when it gets updated.
    :param ids_prices:
    :param post_id_urls:
    :return:
    """
    retries = 0
    while retries < 5:
        post_ids_to_scrape = api_service.get_needs_scrape(ids_prices)
        if post_ids_to_scrape is not None: break
        sleeper.sleep(1, 3)
        print(f'get_needs_scrape Failed. Retrying: {retries}, {ids_prices}')
        slack.send_status_update(f'get_needs_scrape Failed. Retrying: {retries}, {ids_prices}')
        retries = retries + 1

    if post_ids_to_scrape is None:
        raise Exception(f'Could not get post_ids_to_scrape from API')

    id_prices = {entry['postId']: entry['price'] for entry in ids_prices}

    # posts_to_create = []
    count = 0
    for post_id in post_ids_to_scrape:
        # sleeper.sleep_ms()
        url = post_id_urls[post_id]
        if count % 30 == 0:
            logger.info(f'Processing post URL: {url}')
            print(f'P post: {post_id}')
        count = count + 1
        price = id_prices[post_id]
        page = get_page_from_url(url)
        try:
            logger.info(url, zip, price)
            post_data = scraper_service.get_post_data_post_page(page, url, zip, price)
            api_service.create_posts([post_data])
        except Exception as e:
            status = scraper_service.get_page_status(page)
            err = sys.exc_info()[0]
            if status == PageStatusEnum.ACTIVE:
                print(e, err, sys.exc_info())
                logger.info(f'Error processing post: {url}, status: {status}, e: {repr(e)}, error: {repr(err)}')
                raise e
            else:
                print(e, err)
                logger.info(f'Page was no longer available. Status: {status}, error: {repr(e)}')

        # posts_to_create.append(post_data)

    # if i create posts immediately, then if there's a failure, it won't have to recreate
    # the ones it finished prior
    # api_service.create_posts(posts_to_create)

def process_urls(query, urls):
    """
    Handles the updating of the reason when and why posts were removed.
    This happens when the db has a url that what's passed in doesn't.
    :param query: for now it's just {zip: 11111}
    :param urls:
    :return:
    """

    retries = 0
    while retries < 5:
        needs_check_urls = api_service.get_urls_to_check(query, urls)
        if needs_check_urls is not None: break
        sleeper.sleep(1, 3)
        print(f'get_needs_scrape Failed. Retrying: {retries}, query={query} urls={urls}')
        slack.send_status_update(f'get_needs_scrape Failed. Retrying: {retries}, query={query} urls={urls}')
        retries = retries + 1

    if needs_check_urls is None:
        raise Exception(f'Could not get needs_check_urls from API')

    needs_check_urls = api_service.get_urls_to_check(query, urls)

    # updates = []
    for url in needs_check_urls:
        # sleeper.sleep_ms()
        page = get_page_from_url(url)
        status = scraper_service.get_page_status(page)
        update = None
        if status == PageStatusEnum.REMOVED:
            update = {'postId': scraper_service.get_post_id_from_post_url(url), 'removedTime': int(time.time())}
        elif status == PageStatusEnum.EXPIRED:
            update = {'postId': scraper_service.get_post_id_from_post_url(url), 'expiredTime': int(time.time())}
        elif status == PageStatusEnum.ACTIVE:
            # I figured out why this keeps happening. Some zips have more than 3000 posts. So when this happens my db
            # might have posts that doesn't get returned from the site.
            logger.info('***** URL page was still active, but it did not show up on query page.')
            # slack.send_text_alert('URL page was still active, but it did not show up on query page.')
        if update:
            # updates.append(update)
            api_service.update_posts([update])

    # if updates:
    #     api_service.update_posts(updates)


if __name__ == '__main__':
    url = 'https://miami.craigslist.org/mdc/apa/d/hialeah-1-dormitorio-1-bao-disponible/6965603022.html'
    page = get_page_from_url(url)
    status = scraper_service.get_page_status(page)
    print('status', status)
    # process_zip(90026)
