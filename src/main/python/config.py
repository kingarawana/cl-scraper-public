import os
from pathlib import Path
from dotenv import load_dotenv

ENV = os.getenv("SCRAPER_ENV")
env_file_name = 'dev.env'
if ENV and ENV.lower() != 'dev':
    env_file_name = 'prod.env'

if ENV and ENV.lower() == 'test':
    env_file_name = 'test.env'

dir_path = os.path.dirname(os.path.realpath(__file__))
# print('the path: ', dir_path)
# env_path = Path(dir_path + '/../../../') / env_file_name

env_path = Path(dir_path) / env_file_name
load_dotenv(dotenv_path=env_path)


def is_prod():
    return get('ENV') == 'prod'


def is_dev():
    return get('ENV') == 'dev'


def get(key):
    return os.getenv(key)
