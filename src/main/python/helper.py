def np_arrange(start, stop, step):
    value = start
    results = [value]
    stop = stop - 1 # i'm mimicking np.arrange
    for i in range(int(stop/step)):
        value = value + step
        results.append(value)
    return results
