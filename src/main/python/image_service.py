import s3
import sleeper

S3_URL = 'https://real-estate-scraper.s3-us-west-2.amazonaws.com/images/cl/{post_id}/{file_name}'


def save_cl_images_from_urls(urls, post_id):
    return urls
    # disabling for now
    # base_dir = f'images/cl/{post_id}/'
    # s3_urls = []
    # for url in urls:
    #     sleeper.sleep()
    #     file_name = _get_file_name_from_url(url)
    #     s3.copy_image_to_bucket(url, base_dir + file_name)
    #     s3_urls.append(S3_URL.format(post_id=post_id, file_name=file_name))
    # return s3_urls


def _get_file_name_from_url(url):
    return url.split('/')[-1]


# x = ['https://images.craigslist.org/00101_g7R9pbk0rEV_600x450.jpg', 'https://images.craigslist.org/00909_htf3HOx9l7K_600x450.jpg', 'https://images.craigslist.org/01212_lgDBhb4aeK8_600x450.jpg', 'https://images.craigslist.org/01515_fsZOXjGifaA_600x450.jpg', 'https://images.craigslist.org/00606_lRuz1qfAWjC_600x450.jpg', 'https://images.craigslist.org/00303_4CXZbANZFXn_600x450.jpg', 'https://images.craigslist.org/01313_kELV5hKw0oI_600x450.jpg', 'https://images.craigslist.org/00K0K_lqpuP37LX5g_600x450.jpg', 'https://images.craigslist.org/00H0H_i71VPfGGFMz_600x450.jpg', 'https://images.craigslist.org/00l0l_aRwlVV3y3yj_600x450.jpg', 'https://images.craigslist.org/00r0r_iPieYQuBt82_600x450.jpg', 'https://images.craigslist.org/00g0g_1LpZbeO4at_600x450.jpg', 'https://images.craigslist.org/00K0K_kZ5LmGYAp0g_600x450.jpg', 'https://images.craigslist.org/00I0I_luPWbGTWwU9_600x450.jpg', 'https://images.craigslist.org/00s0s_iKqPmV0ZIc0_600x450.jpg', 'https://images.craigslist.org/00G0G_iTxfWbg1y4v_600x450.jpg', 'https://images.craigslist.org/00p0p_dMOgSw7APOe_600x450.jpg', 'https://images.craigslist.org/00404_bnVrjQ8W8vp_600x450.jpg', 'https://images.craigslist.org/00202_8oUDiopUKWb_600x450.jpg', 'https://images.craigslist.org/00y0y_cgMrEnBWHDq_600x450.jpg', 'https://images.craigslist.org/01515_vW5eXhnT2H_600x450.jpg', 'https://images.craigslist.org/00u0u_cdvHs5HI416_600x450.jpg', 'https://images.craigslist.org/00W0W_j1pRR8O6mhy_600x450.jpg']

if __name__ == '__main__':
    # results = save_cl_images_from_urls(x, 11111)
    # print(results)
    pass