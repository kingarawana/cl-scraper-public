import os


def get_test_resources_dir():
    path = os.path.dirname(__file__)
    parts = path.split('/')
    idx = parts.index('unittest')
    path = '/'.join(parts[0: idx+1])
    path = path + '/resources'
    return path


def get_test_resources_file_path(fileName):
    return get_test_resources_dir() + f'/{fileName}'