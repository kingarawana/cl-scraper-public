import unittest
import scraper_service
import zip_processor
import api_service
import time
import slack
import logger
import sleeper
from bs4 import BeautifulSoup
from scraper_service import PageStatusEnum
from mockito import when, any, verify, unstub
from mockito.matchers import ArgumentCaptor


def get_page(file):
    with open(file, 'r') as f:
        page = BeautifulSoup(f.read(), 'html.parser')
    return page


class TestBaseAssembler(unittest.TestCase):

    def setUp(self):
        when(sleeper).sleep()
        when(slack).send_text_alert(any())

    def tearDown(self):
        unstub()

    def test_process_urls_when_removed(self):
        input = ['http://url.com/1']
        query = {'zip': '90026'}

        when(api_service).get_urls_to_check(any(dict), any(list)).thenReturn(input)
        when(zip_processor).get_page_from_url(any(str)).thenReturn({})
        when(scraper_service).get_page_status(any()).thenReturn(PageStatusEnum.REMOVED)

        captor = ArgumentCaptor(any(list))
        when(api_service).update_posts(captor)
        zip_processor.process_urls(query, input)
        self.assertEqual(1, len(captor.value))
        resp_obj = captor.value[0]
        self.assertEqual('1', resp_obj['postId'])
        self.assertTrue(resp_obj['removedTime'] < time.time() + 1)
        verify(api_service, times=1).update_posts(any(list))

    def test_process_urls_when_expired(self):
        input = ['http://url.com/1']
        query = {'zip': '90026'}

        when(api_service).get_urls_to_check(any(dict), any(list)).thenReturn(input)
        when(zip_processor).get_page_from_url(any(str)).thenReturn({})
        when(scraper_service).get_page_status(any()).thenReturn(PageStatusEnum.EXPIRED)

        captor = ArgumentCaptor(any(list))
        when(api_service).update_posts(captor)

        zip_processor.process_urls(query, input)
        self.assertEqual(1, len(captor.value))
        resp_obj = captor.value[0]
        self.assertEqual('1', resp_obj['postId'])
        self.assertTrue(resp_obj['expiredTime'] < time.time() + 1)
        verify(api_service, times=1).update_posts(any(list))

    def test_process_urls_when_active(self):
        input = ['http://url.com/1']
        query = {'zip': '90026'}

        when(api_service).get_urls_to_check(any(dict), any(list)).thenReturn(input)
        when(zip_processor).get_page_from_url(any(str)).thenReturn({})
        when(scraper_service).get_page_status(any()).thenReturn(PageStatusEnum.ACTIVE)

        captor = ArgumentCaptor(any(list))
        when(api_service).update_posts(captor)
        when(logger).error(any())
        zip_processor.process_urls(query, input)
        self.assertIsNone(captor.value)

        # no longer sending slack msgs since I figured out this isn't a must fix problem. Some
        # zips have more than 3000 listings so CL just truncates the rest.
        # verify(slack, times=1).send_text_alert(any())
        verify(api_service, times=0).update_posts(any(list))

    def test_process_posts(self):
        post_id = '1234'
        post_url = 'http://cl.com/1'
        zip = '90026'
        id_prices = [{'postId': post_id, 'price': 3000}]
        post_id_urls = {post_id: post_url}
        when(api_service).get_needs_scrape(id_prices).thenReturn([post_id])
        when(zip_processor).get_page_from_url(post_url).thenReturn({})
        page_captor = ArgumentCaptor(any(dict))
        url_captor = ArgumentCaptor(any(str))
        zip_captor = ArgumentCaptor(any(str))
        price_captor = ArgumentCaptor(any(int))
        post_data = {'some':'data'}

        when(scraper_service).get_post_data_post_page(page_captor, url_captor, zip_captor, price_captor)\
            .thenReturn(post_data)

        post_captor = ArgumentCaptor(any(list))
        when(api_service).create_posts(post_captor)
        zip_processor.process_posts(id_prices, post_id_urls, zip)

        self.assertEqual([post_data], post_captor.value)


if __name__ == '__main__':
    unittest.main()