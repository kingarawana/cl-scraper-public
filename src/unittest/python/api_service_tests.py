import unittest
import api_service
import json
import requests
from mockito import when, any, unstub
from mockito.matchers import ArgumentCaptor
from requests import Response

SUB_QUERY = '?sub=cl'

class TestBaseAssembler(unittest.TestCase):

    def tearDown(self):
        unstub()

    def test_get_next_zip(self):
        resp_data = {'data': 'some data'}
        j = json.dumps(resp_data)

        captor_url = ArgumentCaptor(any(str))
        resp = Response()
        resp.status_code = 200
        resp._content = j.encode('UTF-8')
        when(requests).get(captor_url).thenReturn(resp)
        result = api_service.get_next_zip()
        self.assertEqual(resp_data['data'], result)
        self.assertEqual(captor_url.value, 'http://localhost:3000/realestate/craigslist/zip/next' + SUB_QUERY)

    def test_get_needs_scrape(self):
        resp_data = {'data': 'some data'}
        j = json.dumps(resp_data)

        posts = [{'postId': '1', 'price': 1000}, {'postId': '2', 'price': 2222}]
        captor_url = ArgumentCaptor(any(str))
        captor_data = ArgumentCaptor(any(list))
        resp = Response()
        resp.status_code = 200
        resp._content = j.encode('UTF-8')
        when(requests).post(captor_url, json=captor_data).thenReturn(resp)
        result = api_service.get_needs_scrape(posts)

        self.assertEqual(resp_data['data'], result)
        self.assertEqual(captor_url.value, 'http://localhost:3000/realestate/craigslist/post/scrape' + SUB_QUERY)
        self.assertEqual(captor_data.value, posts)

    def test_mark_zip_complete(self):
        resp_data = {'data': 'some data'}
        j = json.dumps(resp_data)

        zip_id = '19999'
        captor_url = ArgumentCaptor(any(str))
        captor_data = ArgumentCaptor(any(list))
        resp = Response()
        resp.status_code = 200
        resp._content = j.encode('UTF-8')
        when(requests).put(captor_url).thenReturn(resp)
        result = api_service.mark_zip_complete(zip_id)

        self.assertEqual(resp_data['data'], result)
        self.assertEqual(captor_url.value, f'http://localhost:3000/realestate/craigslist/zip/{zip_id}/complete' + SUB_QUERY)

    def test_update_posts(self):
        resp_data = {'data': 'some data'}
        j = json.dumps(resp_data)

        posts = [{'post': '1'}, {'post': '2'}]
        captor_url = ArgumentCaptor(any(str))
        captor_data = ArgumentCaptor(any(list))
        resp = Response()
        resp.status_code = 200
        resp._content = j.encode('UTF-8')
        when(requests).put(captor_url, json=captor_data).thenReturn(resp)
        result = api_service.update_posts(posts)

        self.assertEqual(resp_data['data'], result)
        self.assertEqual(captor_url.value, 'http://localhost:3000/realestate/craigslist/post' + SUB_QUERY)
        self.assertEqual(captor_data.value, posts)

    def test_create_posts(self):
        resp_data = {'data': 'some data'}
        j = json.dumps(resp_data)

        posts = [{'post': '1'}, {'post': '2'}]
        captor_url = ArgumentCaptor(any(str))
        captor_data = ArgumentCaptor(any(list))
        resp = Response()
        resp.status_code = 200
        resp._content = j.encode('UTF-8')
        when(requests).post(captor_url, json=captor_data).thenReturn(resp)
        result = api_service.create_posts(posts)

        self.assertEqual(resp_data['data'], result)
        self.assertEqual(captor_url.value, 'http://localhost:3000/realestate/craigslist/post' + SUB_QUERY)
        self.assertEqual(captor_data.value, posts)

    def test_get_urls_to_check(self):
        resp_data = {'data': 'some data'}
        j = json.dumps(resp_data)

        query = {'zip': '1111'}
        urls = ['url1', 'url2']
        captor_url = ArgumentCaptor(any(str))
        captor_data = ArgumentCaptor(any(dict))
        resp = Response()
        resp.status_code = 200
        resp._content = j.encode('UTF-8')
        when(requests).post(captor_url, json=captor_data).thenReturn(resp)
        result = api_service.get_urls_to_check(query, urls)

        self.assertEqual(resp_data['data'], result)
        self.assertEqual(captor_url.value, 'http://localhost:3000/realestate/craigslist/active' + SUB_QUERY)
        self.assertEqual(captor_data.value, {'postUrls': urls, 'query': query})


if __name__ == '__main__':
    unittest.main()