import service
import logger
import slack
import unittest
import zip_processor
import api_service
import sleeper
from mockito import when, any, verify, unstub
from unittest.mock import Mock
from importlib import reload


class TestBaseAssembler(unittest.TestCase):

    def setUp(self):
        when(sleeper).sleep()

    def tearDown(self):
        unstub()
        reload(api_service)

    def test_handler(self):
        zip1 = {'zip': '90026', '_id': '111', 'clSubDomain': 'losangeles'}
        zip2 = {'zip': '91754', '_id': '222', 'clSubDomain': 'losangeles'}
        zip3 = {'zip': '90210', '_id': '333', 'clSubDomain': 'losangeles'}

        api_service.get_next_zip = Mock(side_effect=[zip1, zip2, zip3, None])

        when(zip_processor).process_zip(zip1['zip'], zip1['clSubDomain'])
        when(zip_processor).process_zip(zip2['zip'], zip1['clSubDomain'])
        when(zip_processor).process_zip(zip3['zip'], zip1['clSubDomain'])

        when(api_service).mark_zip_complete(zip1['_id'])
        when(api_service).mark_zip_complete(zip2['_id'])
        when(api_service).mark_zip_complete(zip3['_id'])

        service.handler(None, None)

        verify(zip_processor, times=1).process_zip(zip1['zip'], zip1['clSubDomain'])
        verify(zip_processor, times=1).process_zip(zip2['zip'], zip1['clSubDomain'])
        verify(zip_processor, times=1).process_zip(zip3['zip'], zip1['clSubDomain'])

        verify(api_service, times=1).mark_zip_complete(zip1['_id'])
        verify(api_service, times=1).mark_zip_complete(zip2['_id'])
        verify(api_service, times=1).mark_zip_complete(zip3['_id'])
        verify(api_service, times=3).mark_zip_complete(any())

    def xtest_handler_has_error(self):
        """
        I've disabled this because we are not eating the errors/have a new error
        handling process. Will fix this later.
        :return:
        """
        zip1 = {'zip': '90026', '_id': '111'}

        api_service.get_next_zip = Mock(side_effect=[zip1, None])

        err = Exception('Some Error')
        when(zip_processor).process_zip(zip1['zip']).thenRaise(err)
        when(api_service).mark_zip_complete(any())
        when(logger).error(any(), any(), any()).thenReturn(None)
        when(slack).send_text_alert(any()).thenReturn(None)

        with self.assertRaises(Exception) as context:
            service.handler(None, None)

        print(context.exception)
        self.assertEqual(context.exception, err)
        verify(zip_processor, times=1).process_zip(zip1['zip'])
        verify(api_service, times=0).mark_zip_complete(any())
        verify(logger, times=1).error(any(), any(), any())
        verify(slack, times=1).send_text_alert(any())


if __name__ == '__main__':
    unittest.main()