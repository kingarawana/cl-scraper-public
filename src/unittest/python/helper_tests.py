import unittest
import helper
import numpy as np
import random


class TestBaseAssembler(unittest.TestCase):

    def test_arrange(self):
        results_total = 500
        start = 0
        step = 120
        np_pages = list(np.arange(start, results_total + 1, step))
        hp_pages = helper.np_arrange(start, results_total + 1, step)
        print(np_pages, hp_pages)
        self.assertEqual(np_pages, hp_pages)

    def test_arrange(self):
        results_total = 119
        start = 0
        step = 120
        np_pages = list(np.arange(start, results_total + 1, step))
        hp_pages = helper.np_arrange(start, results_total + 1, step)
        self.assertEqual(np_pages, hp_pages)

    def test_arrange_randomized(self):
        start = 0
        step = 120

        for i in range(1000):
            results_total = random.randint(0, 500)
            np_pages = list(np.arange(start, results_total + 1, step))
            hp_pages = helper.np_arrange(start, results_total + 1, step)
            self.assertEqual(np_pages, hp_pages)


if __name__ == '__main__':
    unittest.main()