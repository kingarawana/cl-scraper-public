import unittest
from bs4 import BeautifulSoup
import scraper_service
import test_helper
import image_service
from mockito import when, unstub, any
from scraper_service import PageStatusEnum


def get_page(file):
    with open(file, 'r') as f:
        page = BeautifulSoup(f.read(), 'html.parser')
    return page


class TestBaseAssembler(unittest.TestCase):

    def tearDown(self):
        unstub()

    def test_get_images(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        images = scraper_service.get_images(get_page(test_file_path))
        self.assertTrue(images)
        self.assertEqual(23, len(images))

    def test_get_description(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        descripton = scraper_service.get_description(get_page(test_file_path))
        self.assertEqual(len(descripton), 910)

    def test_get_tags(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        tags = scraper_service.get_tags(get_page(test_file_path))
        expected = ['2BR / 1Ba', '850ft2', 'available sep 25', 'apartment', 'laundry on site', 'no smoking', 'carport', 'wheelchair accessible']
        self.assertEqual(expected, tags)

    def test_get_parking_type_from_tags(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        tags = scraper_service.get_tags(get_page(test_file_path))
        parking_type = scraper_service.get_parking_type_from_tags(tags)
        self.assertEqual('CARPORT', parking_type)

    def test_get_details(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        details = scraper_service.get_details(get_page(test_file_path))
        self.assertTrue(details)
        self.assertEqual(len(details), 4)

    def test_get_address(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        address = scraper_service.get_address(get_page(test_file_path))
        self.assertEqual('2715 James M. Wood Blvd', address)

    def test_get_lat_long(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        lat_long = scraper_service.get_lat_long(get_page(test_file_path))
        self.assertEqual(['34.056011', '-118.287087'], lat_long)

    def test_get_page_status_existing(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        status = scraper_service.get_page_status(get_page(test_file_path))
        self.assertEqual(PageStatusEnum.ACTIVE, status)

    def test_get_page_status_expired(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page_expired.html')
        status = scraper_service.get_page_status(get_page(test_file_path))
        self.assertEqual(PageStatusEnum.EXPIRED, status)

    def test_get_page_status_removed_missing(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_missing.html')
        status = scraper_service.get_page_status(get_page(test_file_path))
        self.assertEqual(PageStatusEnum.MISSING, status)

    def test_get_page_status_removed(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page_removed.html')
        status = scraper_service.get_page_status(get_page(test_file_path))
        self.assertEqual(PageStatusEnum.REMOVED, status)

    def test_get_post_date_in_seconds(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        seconds = scraper_service.get_post_date_in_seconds(get_page(test_file_path))
        self.assertEqual(1569287559, seconds)

    def test_get_posts_from_search_page(self):
        test_file_path = test_helper.get_test_resources_file_path('search_results_page.html')
        search_page = get_page(test_file_path)
        posts = scraper_service.get_posts_from_search_page(search_page)
        self.assertEqual(30, len(posts))

    def test_get_posts_from_search_page(self):
        test_file_path = test_helper.get_test_resources_file_path('search_results_page.html')
        search_page = get_page(test_file_path)
        posts = scraper_service.get_posts_from_search_page(search_page)
        self.assertEqual(30, len(posts))

    def test_get_price_from_post(self):
        test_file_path = test_helper.get_test_resources_file_path('search_results_page.html')
        search_page = get_page(test_file_path)
        posts = scraper_service.get_posts_from_search_page(search_page)
        post = posts[0]
        price = scraper_service.get_price_from_post(post)
        self.assertEqual(1850, price)

    def test_get_price_from_post_no_span_price(self):
        test_file_path = test_helper.get_test_resources_file_path('search_results_page.html')
        search_page = get_page(test_file_path)
        posts = scraper_service.get_posts_from_search_page(search_page)
        post = posts[1]
        price = scraper_service.get_price_from_post(post)
        self.assertEqual(1111, price)

    def test_get_post_url_from_post(self):
        test_file_path = test_helper.get_test_resources_file_path('search_results_page.html')
        search_page = get_page(test_file_path)
        posts = scraper_service.get_posts_from_search_page(search_page)
        post = posts[0]
        url = scraper_service.get_post_url_from_post(post)
        self.assertEqual('https://losangeles.craigslist.org/lac/apa/d/los-angeles-2-bed-1-bath-in-korea-town/6982616161.html', url)

    def test_get_post_id_from_post_url(self):
        test_file_path = test_helper.get_test_resources_file_path('search_results_page.html')
        search_page = get_page(test_file_path)
        posts = scraper_service.get_posts_from_search_page(search_page)
        post = posts[0]
        url = scraper_service.get_post_url_from_post(post)
        post_id = scraper_service.get_post_id_from_post_url(url)
        self.assertEqual('6982616161', post_id)

    def test_get_post_data_post_page(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_page.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$1850 / 2br - 850ft2 - Welcome home to an amazing 2 bed 1 bath only $1850 mo (2715 W James M. Wood Blvd) ', 'description': '\\n        \\n            QR Code Link to This Post\\n            \\n        \\nThe South Tower Jameswood Apartments is conveniently located in Korea Town, close to fwys 101, 110, 10 with a peaceful feel courtyard that provides a warm welcome every time you come home. Very comfortable living\\n\\nThe unit has centralized air conditioner hot and cold. \\nThe stove and dishwasher are included.\\nGreat closet space. \\nall newly remodeled with new tile flooring\\nNice, clean, gated building with manager on site.\\nElevator access , keyed entry ,\\ncontrolled garage gate access, \\nLaundry room in each floor(3 floors)\\n\\nrequirements for application process\\nproof of income to total min 2 1/2 x the monthy rent\\npaystubs of current employement 2 months\\nCAID or DL & SSN\\n$30 processing fee per adult applicant\\n$1850 monthly rent\\n$1850 security deposit\\nplease call  show contact info\\n johana\\nCome by today also    ', 'street': '2715 James M. Wood Blvd', 'zip': 90026, 'latLong': ['34.056011', '-118.287087'], 'price': 2999, 'numBedrooms': 2, 'numBaths': 1.0, 'sqft': 850, 'parkingType': 'CARPORT', 'dateAvailable': 1569394800, 'postDate': 1569287559, 'images': ['https://image.com/1'], 'tags': ['apartment', 'laundry on site', 'no smoking', 'carport', 'wheelchair accessible']}
        self.assertEqual(expected, page_obj)

    def test_get_post_data_post_page(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_missing_tags.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$9010 / 3br - 1790ft2 - Particular architectural hous (Silver Lake) ', 'description': "\\n        \\n            QR Code Link to This Post\\n            \\n        \\nParticular architectural house concealed amongst towering cedars on a hillside at Echo Park. Finished in 2015, this three bedroom/2-bath comes with a tree growing up through the ground of the next bedroom. The house provides impressive quiet and seclusion, athletic reclaimed wood, hand-made tiles, along with floor-to-ceiling windows. Across the road are Elysian Park\\'s grand hiking paths, and a number of thebest cafes and stores in town. The place gives rapid access to downtown.This enchanting home was profiled in style magazines such as Wallpaper, and featured on television. Retractable glass walls and a wooded breezeway independent another living room, second kitchen plus third bedroom which could be utilized as an independent mother-in-law, or incorporated into the primary living area. Unusual opportunity.\\nPropID.7355JD    ", 'street': None, 'zip': 90026, 'latLong': ['34.076600', '-118.264600'], 'price': 2999, 'numBedrooms': 3, 'numBaths': 2.0, 'sqft': 1790, 'parkingType': '', 'dateAvailable': None, 'postDate': 1569979312, 'images': ['https://image.com/1'], 'tags': ['3BR / 2Ba', '1790ft2', 'house']}

        self.assertEqual(expected, page_obj)

    def test_get_post_data_post_page_open_house(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_missing_tags.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$9010 / 3br - 1790ft2 - Particular architectural hous (Silver Lake) ', 'description': "\\n        \\n            QR Code Link to This Post\\n            \\n        \\nParticular architectural house concealed amongst towering cedars on a hillside at Echo Park. Finished in 2015, this three bedroom/2-bath comes with a tree growing up through the ground of the next bedroom. The house provides impressive quiet and seclusion, athletic reclaimed wood, hand-made tiles, along with floor-to-ceiling windows. Across the road are Elysian Park\\'s grand hiking paths, and a number of thebest cafes and stores in town. The place gives rapid access to downtown.This enchanting home was profiled in style magazines such as Wallpaper, and featured on television. Retractable glass walls and a wooded breezeway independent another living room, second kitchen plus third bedroom which could be utilized as an independent mother-in-law, or incorporated into the primary living area. Unusual opportunity.\\nPropID.7355JD    ", 'street': None, 'zip': 90026, 'latLong': ['34.076600', '-118.264600'], 'price': 2999, 'numBedrooms': 3, 'numBaths': 2.0, 'sqft': 1790, 'parkingType': '', 'dateAvailable': None, 'postDate': 1569979312, 'images': ['https://image.com/1'], 'tags': ['3BR / 2Ba', '1790ft2', 'house']}
        self.assertEqual(expected, page_obj)

    def test_get_post_complex_1(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_complex_1.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': "\\n$2200 / 1br - 450ft2 - LUXURY 1-BED, 1-BATH APT IN HOT 'HOOD (SILVER LAKE) ", 'description': "\\n        \\n            QR Code Link to This Post\\n            \\n        \\nAvailable now: A beautiful recently-renovated one bedroom, one bathroom apartment with stunning views in the heart of one of America\\'s hippest and most happening neighbourhoods. Leafy gated compound, secure controlled access, hardwood floors, full kitchen, modern appliances including your very own washer and dryer.  Easy access to nearby shops and restaurants, including Cafe Stella, Intelligentsia Coffee at Silver Lake\\'s famous Sunset Junction,  and a weekly Farmers\\' Market. Hollywood is 10 mins away, Downtown 15 mins away. Close to all major freeways including the 101, 5, 10 and 2 freeways. \\n\\nAvailable from the 5th of October 2019.\\nOne year lease.\\n$2,200 a month + one month\\'s rent as security deposit. \\nFurniture not included.\\nTenant pays gas and electricity. \\nDog & cat/pet friendly.\\nGood credit and references required. \\n$30 screening/credit check fee. \\nViewing by appointment only.    ", 'street': None, 'zip': 90026, 'latLong': ['34.083710', '-118.260007'], 'price': 2999, 'numBedrooms': 1, 'numBaths': 1.0, 'sqft': 450, 'parkingType': 'ATTACHED_GARAGE', 'dateAvailable': 1570172400.0, 'postDate': 1569979632, 'images': ['https://image.com/1'], 'tags': ['1BR / 1Ba', '450ft2', 'available oct 4', '\\n                        saturday 2019-10-05\\n', '\\n                        sunday 2019-10-06\\n', '\\n                        monday 2019-10-07\\n', 'application fee details: $30 credit check, processing', 'cats are OK - purrr', 'dogs are OK - wooof', 'apartment', 'w/d in unit', 'no smoking', 'attached garage']}

        self.assertEqual(expected, page_obj)

    def test_get_post_baths_text(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_baths_text.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$1200 / 3br - 1282ft2 - My house your home (1650 Angelus Ave, Los Angeles, CA) ', 'description': '\\n        \\n            QR Code Link to This Post\\n            \\n        \\nNestled in the hills of Silver Lake on a beautifully landscaped lot is this updated 3 bedroom, 1 bathroom move-in ready home. The well thought out floor plan lends itself to an indoor/outdoor lifestyle with an inviting living room with fireplace and direct access to the large flat fully hedged private yard with multiple areas to entertain including an outdoor deck. The updated eat-in cooks kitchen features stone counter tops, glass subway tile backsplash, high end stainless steel appliances, and plenty of cabinet space. Additional Features include gleaming hardwood floors, dimmer controlled recessed lighting throughout, plantation shutters, a large 2 car garage, laundry room, copper plumbing, newer roof, and a nest system controlling the central HVAC as well as cameras monitoring the exterior perimeter of the property. Included are approved plans for a 499sf addition with a bonus rooftop deck. A true Silver Lake gem surrounded by multi-million dollar homes!    ', 'street': '1650 Angelus Ave, Los Angeles', 'zip': 90026, 'latLong': ['34.076600', '-118.264600'], 'price': 2999, 'numBedrooms': 3, 'numBaths': -1, 'sqft': 1282, 'parkingType': 'DETACHED_GARAGE', 'dateAvailable': None, 'postDate': 1569645154, 'images': ['https://image.com/1'], 'tags': ['3BR / splitBa', '1282ft2', 'cats are OK - purrr', 'dogs are OK - wooof', 'furnished', 'house', 'laundry in bldg', 'detached garage']}
        self.assertEqual(expected, page_obj)

    def test_get_post_complex_2(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_complex_2.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n2br - LOS ANGELES DE RENTA 2 RECAMARAS, SEC 8 OK (LOS ANGELES) ', 'description': '\\n        \\n            QR Code Link to This Post\\n            \\n        \\namplio apartamento de renta 2 recamaras,en buena area de sur central,se acepta seccion 8,estacionamiento,llame para mas informacion  show contact info\\n    ', 'street': None, 'zip': 90026, 'latLong': ['33.973100', '-118.247900'], 'price': 2999, 'numBedrooms': 2, 'numBaths': 1.0, 'sqft': None, 'parkingType': 'CARPORT', 'dateAvailable': 1568876400.0, 'postDate': 1568909265, 'images': ['https://image.com/1'], 'tags': ['2BR / 1Ba', 'available sep 19', 'apartment', 'carport']}
        self.assertEqual(expected, page_obj)

    def test_get_post_missing_bedroom_bath(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_missing_bedroom_bath.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$825 Small back unit for rent/lease..! ', 'description': '\\n        \\n            QR Code Link to This Post\\n            \\n        \\nSmall back unit single for rent/lease Utilities included comes with full private bathroom..!\\nDoesn’t have a kitchen..!!!!\\nDeposit 350$\\nMonthly 825\\nP.s:You would be 10 minutes away from usc and also downtown LA    ', 'street': None, 'zip': 90026, 'latLong': ['34.007900', '-118.258200'], 'price': 2999, 'numBedrooms': None, 'numBaths': None, 'sqft': None, 'parkingType': '', 'dateAvailable': None, 'postDate': 1570134342, 'images': ['https://image.com/1'], 'tags': ['loft', 'no smoking']}
        self.assertEqual(expected, page_obj)

    def test_get_post_complex_3(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_complex_3.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$2025 / 478ft2 - October Availability-DTLA-Pool view Studio with free parking! (DTLA/Los Angeles-View today-Xtra Credits-Call Geoff) ', 'description': '\\n        \\n            QR Code Link to This Post\\n            \\n        \\nWant the convenience of living Downtown without the hassle? The Da Vinci is your exclusive key to the city. \\n\\nLocated in the heart of Downtown Los Angeles, you will be greeted by a community of luxury apartment homes that feature everything from full amenities to easy access to a premier shopping and dining culture. And with close proximity to multiple freeways and local public transportation, including bus and subway lines, you never have to stress about exploring your city. \\n\\nWith a variety of floor plans available, the Da Vinci has a home just for you. We encourage you to contact us at  show contact info\\n for a free tour, available deals, and any additional questions you may have. \\n\\nAPARTMENT FEATURES\\n\\n•\\tBalcony or Patio with Beautiful Views \\n•\\tCentral Air and Heat \\n•\\tHardwood Floors or Lush Carpets \\n•\\tHigh-Speed Internet Access (Wi-Fi Connectivity) \\n•\\tFull-Sized Washer Dryer in Unit \\n•\\tAnd many more… \\n\\nCOMMUNITY AMENITIES\\n\\n•\\tFull Size Indoor Basketball Court \\n•\\tDirector’s Movie Screening Theater and Karaoke Room \\n•\\tGated Private Parking \\n•\\tRooftop Pool with Skyline Views \\n•\\tState-of-the-Art Fitness Center \\n•\\tAnd much more… \\n\\nCONTACT\\n\\n show contact info\\n \\n\\nWEBSITE\\n\\nwww.thedavinciapts.com \\n\\nADDRESS\\n\\n909 W. Temple Street; Los Angeles, CA 90012 \\n    ', 'street': '909 W. Temple Street', 'zip': 90026, 'latLong': ['34.058917', '-118.249111'], 'price': 2999, 'numBedrooms': 0, 'numBaths': 1.0, 'sqft': 478, 'parkingType': 'ATTACHED_GARAGE', 'dateAvailable': 1570086000.0, 'postDate': 1570141352, 'images': ['https://image.com/1'], 'tags': ['0BR / 1Ba', '478ft2', 'available oct 3', '\\n                        thursday 2019-10-03\\n', '\\n                        friday 2019-10-04\\n', '\\n                        saturday 2019-10-05\\n', 'EV charging', 'apartment', 'w/d in unit', 'attached garage', 'wheelchair accessible']}
        self.assertEqual(expected, page_obj)

    def test_get_post_no_tags(self):
        test_file_path = test_helper.get_test_resources_file_path('cl_post_no_tags.html')
        post_id = '1234'
        zip = 90026
        url = f'https://cl.com/{post_id}'
        price = 2999
        when(image_service).save_cl_images_from_urls(any(), post_id).thenReturn(['https://image.com/1'])
        page_obj = scraper_service.get_post_data_post_page(get_page(test_file_path), url, zip, price)
        print(page_obj)
        expected = {'postId': '1234', 'postUrl': 'https://cl.com/1234', 'title': '\\n$5300 Apartment for Lease ((move in anytime)) ', 'description': '\\n        \\n            QR Code Link to This Post\\n            \\n        \\n507 Glenrock Regency is a student housing complex walking distance from UCLA campus. Glenrock Regency is offering 2bd/2bth units for the upcoming school year that hold a maximum of 5 students per unit. Units are to be rented by bed space starting at $785/bed space This complex is for students only. Open but not limited to students that go to UCLA, UCLA extension, SMC, and USC. Units come with central air and heat, floor to ceiling windows creating extremely bright units, and units may have hardwood flooring and in unit washer/dryers . All kitchens come with microwave, refrigerator, stove, and dishwasher. The building also has a 24/7 rooftop pool/jacuzzi. Free WiFi, water, and trash are included.\\n\\nCommunity Amenities:\\nrooftop pool/jacuzzi with 360 views of the city\\ngated covered parking\\n\\nApartment Amenities:\\ngas fireplace \\nprivate balcony\\nhardwood flooring (unit specific)\\nin unit washer/dryer (unit specific)\\nfloor to ceiling sliding glass doors\\nBRIGHT units \\ngas range, refrigerator, microwave, dishwasher\\ncentral air/heat\\n\\nRent: $5300\\n\\n    ', 'street': None, 'zip': 90026, 'latLong': ['34.112100', '-118.259400'], 'price': 2999, 'numBedrooms': None, 'numBaths': None, 'sqft': None, 'parkingType': '', 'dateAvailable': None, 'postDate': 1567605960, 'images': ['https://image.com/1'], 'tags': []}
        self.assertEqual(expected, page_obj)

if __name__ == '__main__':
    unittest.main()