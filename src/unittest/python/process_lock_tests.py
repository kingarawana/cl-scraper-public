import unittest
import process_lock
from unittest.mock import MagicMock
from mockito import any, when, unstub, verify

LOCK_FILE = 'SOME_FILE_PATH'
CURRENT_TIME = 1000


class ProcessLockTest(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        when(process_lock.s3).delete(LOCK_FILE)
        when(process_lock.s3).put_json_object_by_key(LOCK_FILE, any())
        when(process_lock.time).time().thenReturn(CURRENT_TIME)

    def tearDown(self) -> None:
        unstub()

    def test_run_process_if_not_locked_when_no_existing_lock(self):
        locker = process_lock.ProcessLock(LOCK_FILE)
        request_id = '1234'
        some_process = MagicMock(name='locked process')
        when(process_lock.s3).get_json_object_by_key(LOCK_FILE).thenReturn(None)
        expected_lock = {'request_id': request_id, 'timestamp': CURRENT_TIME}

        locker.run_process_if_not_locked(request_id, some_process)

        verify(process_lock.s3).put_json_object_by_key(LOCK_FILE, expected_lock)
        verify(process_lock.s3).delete(LOCK_FILE)
        some_process.assert_called_once()

    def test_run_process_if_existing_lock_same_request_id(self):
        locker = process_lock.ProcessLock(LOCK_FILE)
        request_id = '1234'
        some_process = MagicMock(name='locked process')
        existing_lock = {'request_id': request_id, 'timestamp': CURRENT_TIME - 10}
        when(process_lock.s3).get_json_object_by_key(LOCK_FILE).thenReturn(existing_lock)
        expected_lock = {'request_id': request_id, 'timestamp': CURRENT_TIME}

        locker.run_process_if_not_locked(request_id, some_process)

        verify(process_lock.s3).put_json_object_by_key(LOCK_FILE, expected_lock)
        verify(process_lock.s3).delete(LOCK_FILE)
        some_process.assert_called_once()

    def test_run_process_if_existing_lock_different_request_id_with_timestamp_over_threshold(self):
        locker = process_lock.ProcessLock(LOCK_FILE, 100)
        request_id = '1234'
        some_process = MagicMock(name='locked process')
        other_lock = {'request_id': '123454545', 'timestamp': CURRENT_TIME - 200}
        when(process_lock.s3).get_json_object_by_key(LOCK_FILE).thenReturn(other_lock)

        locker.run_process_if_not_locked(request_id, some_process)

        verify(process_lock.s3, times=0).put_json_object_by_key(LOCK_FILE, any())
        verify(process_lock.s3).delete(LOCK_FILE)
        some_process.assert_not_called()

    def test_run_process_if_existing_lock_different_request_id_with_timestamp_under_threshold(self):
        locker = process_lock.ProcessLock(LOCK_FILE, 100)
        request_id = '1234'
        some_process = MagicMock(name='locked process')
        other_lock = {'request_id': '123454545', 'timestamp': CURRENT_TIME - 50}
        when(process_lock.s3).get_json_object_by_key(LOCK_FILE).thenReturn(other_lock)

        locker.run_process_if_not_locked(request_id, some_process)

        verify(process_lock.s3, times=0).put_json_object_by_key(LOCK_FILE, any())
        verify(process_lock.s3, times=0).delete(LOCK_FILE)
        some_process.assert_not_called()


if __name__ == '__main__':
    unittest.main()
