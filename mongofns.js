function copyFromLocalToProduction(collectionName){
	var myMongo = new Mongo("mongodb://xxxx:27017");
	var prodDb = myMongo.getDB('xxxxx');
	prodDb.auth("xxxx", "ExxxxWw");
	var items = db[collectionName].find()
	if(items.count() > 0){
		items.forEach(function(x){
			prodDb[collectionName].save(x);
		});
	}
	print(items.count());
}


function createImage(){
	var posts = db.realestate.clposts.find();
	objs = []
	posts.forEach(function(post){
		post.images.forEach(function(i){
			objs.push({srcUrl: i, processingRequestTime: 1, finalUrl: null});
		});
	});
	db.realestate.images.insertMany(objs)
}

function findAndUpdate(){
	var key = 'real-estate-scraper';
	var imgs = db.realestate.images.find()
	for(var i = 0, len = imgs.length; i < len; i++){
		if(imgs[i].srcUrl.indexOf(key) > -1){
			var img = imgs[i];
			img.finalUrl = img.srcUrl;
			img.processingRequestTime = 2;
			db.realestate.images.save(img)
		}
	}
}

function findAndUpdate(){
	var key = 'real-estate-scraper';
	var imgs = db.realestate.images.find();
	imgs.forEach(function(img){
		if(img.srcUrl.indexOf(key) > -1){
			img.finalUrl = img.srcUrl;
			img.processingRequestTime = 2;
			db.realestate.images.save(img)
		}
	});
}

/**
added 1 day and 1 minute for it to start the next day at 1 am
**/
function createZips(zips){
    existing = db.realestate.clzips.find({}, {zip:1}).map(z => z.zip)
    zips = zips.filter(z => !existing.includes(z))
    let oneAm = new Date();
    oneAm.setHours(1);
    oneAm.setMinutes(0);
    oneAm.setSeconds(0);
    oneAm.setMilliseconds(0);
    let create = zips.map(zip => {
        return {
            zip: zip,
            lastCompletedProcess: 0,
            processingRequestTime: oneAm.getTime() + (86400000 + 60000),
            __v: 0
        }
    });
    db.realestate.clzips.insertMany(create)
}

function createZips(zips){
    existing = db.realestate.clzips.find({}, {zip:1}).map(z => z.zip)
    zips = zips.filter(z => !existing.includes(z))
    let create = zips.map(zip => {
        return {
            zip: zip,
            lastCompletedProcess: 1,
            processingRequestTime: 1,
            __v: 0
        }
    });
    db.realestate.clzips.insertMany(create)
}


realestate.clposts



mongodb://api.xxxxxapp.com:27017

var myMongo = new Mongo("mongodb://xxxxx:27017");


sudo vi /etc/mongod.conf
  131  sudo service mongodb restart
  132  sudo service mongod restart



var objectIdFromDate = function (date) {
	return Math.floor(date.getTime() / 1000).toString(16) + "0000000000000000";
};

var dateFromObjectId = function (objectId) {
	return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};

function check(){
	let count = 0;
	let d = new Date(2019, 9, 15)
	db.realestate.clzips.find({}, {_id:1, zip:1}).forEach(zip => {
		if(dateFromObjectId(zip._id.valueOf()) > d){
			count++;
			db.realestate.clzips.update({_id: zip._id}, {$set:{disabled:true}})
		}else{
			db.realestate.clzips.update({_id: zip._id}, {$set:{disabled:false}})
		}
	})
	let total = db.realestate.clzips.count();
	let remaining = total - count;
	print('count', count, remaining)
}

function findAllPriorZips(){
	let posts = db.realestate.clposts.find()
	let lineDate = new Date(2019, 9, 15)
	let priorZips = new Set()
	posts.forEach(p => {
		if(new Date(p.creationDate) < lineDate){
			priorZips.add(p.zip)
		}
	})

	pz = Array.from(priorZips);
	db.realestate.clzips.updateMany({zip: {$in:pz}}, {$set:{disabled:false}})
	db.realestate.clzips.updateMany({zip: {$nin:pz}}, {$set:{disabled:true}})
}


craigslist_state_url_mapping = {'Alabama': ['https://auburn.craigslist.org/', 'https://bham.craigslist.org/', 'https://dothan.craigslist.org/', 'https://shoals.craigslist.org/', 'https://gadsden.craigslist.org/', 'https://huntsville.craigslist.org/', 'https://mobile.craigslist.org/', 'https://montgomery.craigslist.org/', 'https://tuscaloosa.craigslist.org/'], 'Alaska': ['https://anchorage.craigslist.org/', 'https://fairbanks.craigslist.org/', 'https://kenai.craigslist.org/', 'https://juneau.craigslist.org/'], 'Arizona': ['https://flagstaff.craigslist.org/', 'https://mohave.craigslist.org/', 'https://phoenix.craigslist.org/', 'https://prescott.craigslist.org/', 'https://showlow.craigslist.org/', 'https://sierravista.craigslist.org/', 'https://tucson.craigslist.org/', 'https://yuma.craigslist.org/'], 'Arkansas': ['https://fayar.craigslist.org/', 'https://fortsmith.craigslist.org/', 'https://jonesboro.craigslist.org/', 'https://littlerock.craigslist.org/', 'https://texarkana.craigslist.org/'], 'California': ['https://bakersfield.craigslist.org/', 'https://chico.craigslist.org/', 'https://fresno.craigslist.org/', 'https://goldcountry.craigslist.org/', 'https://hanford.craigslist.org/', 'https://humboldt.craigslist.org/', 'https://imperial.craigslist.org/', 'https://inlandempire.craigslist.org/', 'https://losangeles.craigslist.org/', 'https://mendocino.craigslist.org/', 'https://merced.craigslist.org/', 'https://modesto.craigslist.org/', 'https://monterey.craigslist.org/', 'https://orangecounty.craigslist.org/', 'https://palmsprings.craigslist.org/', 'https://redding.craigslist.org/', 'https://sacramento.craigslist.org/', 'https://sandiego.craigslist.org/', 'https://sfbay.craigslist.org/', 'https://slo.craigslist.org/', 'https://santabarbara.craigslist.org/', 'https://santamaria.craigslist.org/', 'https://siskiyou.craigslist.org/', 'https://stockton.craigslist.org/', 'https://susanville.craigslist.org/', 'https://ventura.craigslist.org/', 'https://visalia.craigslist.org/', 'https://yubasutter.craigslist.org/'], 'Colorado': ['https://boulder.craigslist.org/', 'https://cosprings.craigslist.org/', 'https://denver.craigslist.org/', 'https://eastco.craigslist.org/', 'https://fortcollins.craigslist.org/', 'https://rockies.craigslist.org/', 'https://pueblo.craigslist.org/', 'https://westslope.craigslist.org/'], 'Connecticut': ['https://newlondon.craigslist.org/', 'https://hartford.craigslist.org/', 'https://newhaven.craigslist.org/', 'https://nwct.craigslist.org/'], 'Delaware': ['https://delaware.craigslist.org/'], 'District of Columbia': ['https://washingtondc.craigslist.org/'], 'Florida': ['http://miami.craigslist.org/brw/', 'https://daytona.craigslist.org/', 'https://keys.craigslist.org/', 'https://fortlauderdale.craigslist.org/', 'https://fortmyers.craigslist.org/', 'https://gainesville.craigslist.org/', 'https://cfl.craigslist.org/', 'https://jacksonville.craigslist.org/', 'https://lakeland.craigslist.org/', 'http://miami.craigslist.org/mdc/', 'https://lakecity.craigslist.org/', 'https://ocala.craigslist.org/', 'https://okaloosa.craigslist.org/', 'https://orlando.craigslist.org/', 'https://panamacity.craigslist.org/', 'https://pensacola.craigslist.org/', 'https://sarasota.craigslist.org/', 'https://miami.craigslist.org/', 'https://spacecoast.craigslist.org/', 'https://staugustine.craigslist.org/', 'https://tallahassee.craigslist.org/', 'https://tampa.craigslist.org/', 'https://treasure.craigslist.org/', 'http://miami.craigslist.org/pbc/'], 'Georgia': ['https://albanyga.craigslist.org/', 'https://athensga.craigslist.org/', 'https://atlanta.craigslist.org/', 'https://augusta.craigslist.org/', 'https://brunswick.craigslist.org/', 'https://columbusga.craigslist.org/', 'https://macon.craigslist.org/', 'https://nwga.craigslist.org/', 'https://savannah.craigslist.org/', 'https://statesboro.craigslist.org/', 'https://valdosta.craigslist.org/'], 'Hawaii': ['https://honolulu.craigslist.org/'], 'Idaho': ['https://boise.craigslist.org/', 'https://eastidaho.craigslist.org/', 'https://lewiston.craigslist.org/', 'https://twinfalls.craigslist.org/'], 'Illinois': ['https://bn.craigslist.org/', 'https://chambana.craigslist.org/', 'https://chicago.craigslist.org/', 'https://decatur.craigslist.org/', 'https://lasalle.craigslist.org/', 'https://mattoon.craigslist.org/', 'https://peoria.craigslist.org/', 'https://rockford.craigslist.org/', 'https://carbondale.craigslist.org/', 'https://springfieldil.craigslist.org/', 'https://quincy.craigslist.org/'], 'Indiana': ['https://bloomington.craigslist.org/', 'https://evansville.craigslist.org/', 'https://fortwayne.craigslist.org/', 'https://indianapolis.craigslist.org/', 'https://kokomo.craigslist.org/', 'https://tippecanoe.craigslist.org/', 'https://muncie.craigslist.org/', 'https://richmondin.craigslist.org/', 'https://southbend.craigslist.org/', 'https://terrehaute.craigslist.org/'], 'Iowa': ['https://ames.craigslist.org/', 'https://cedarrapids.craigslist.org/', 'https://desmoines.craigslist.org/', 'https://dubuque.craigslist.org/', 'https://fortdodge.craigslist.org/', 'https://iowacity.craigslist.org/', 'https://masoncity.craigslist.org/', 'https://quadcities.craigslist.org/', 'https://siouxcity.craigslist.org/', 'https://ottumwa.craigslist.org/', 'https://waterloo.craigslist.org/'], 'Kansas': ['https://lawrence.craigslist.org/', 'https://ksu.craigslist.org/', 'https://nwks.craigslist.org/', 'https://salina.craigslist.org/', 'https://seks.craigslist.org/', 'https://swks.craigslist.org/', 'https://topeka.craigslist.org/', 'https://wichita.craigslist.org/'], 'Kentucky': ['https://bgky.craigslist.org/', 'https://eastky.craigslist.org/', 'https://lexington.craigslist.org/', 'https://louisville.craigslist.org/', 'https://owensboro.craigslist.org/', 'https://westky.craigslist.org/'], 'Louisiana': ['https://batonrouge.craigslist.org/', 'https://cenla.craigslist.org/', 'https://houma.craigslist.org/', 'https://lafayette.craigslist.org/', 'https://lakecharles.craigslist.org/', 'https://monroe.craigslist.org/', 'https://neworleans.craigslist.org/', 'https://shreveport.craigslist.org/'], 'Maine': ['https://maine.craigslist.org/'], 'Maryland': ['https://annapolis.craigslist.org/', 'https://baltimore.craigslist.org/', 'https://easternshore.craigslist.org/', 'https://frederick.craigslist.org/', 'https://smd.craigslist.org/', 'https://westmd.craigslist.org/'], 'Massachusetts': ['https://boston.craigslist.org/', 'https://capecod.craigslist.org/', 'https://southcoast.craigslist.org/', 'https://westernmass.craigslist.org/', 'https://worcester.craigslist.org/'], 'Michigan': ['https://annarbor.craigslist.org/', 'https://battlecreek.craigslist.org/', 'https://centralmich.craigslist.org/', 'https://detroit.craigslist.org/', 'https://flint.craigslist.org/', 'https://grandrapids.craigslist.org/', 'https://holland.craigslist.org/', 'https://jxn.craigslist.org/', 'https://kalamazoo.craigslist.org/', 'https://lansing.craigslist.org/', 'https://monroemi.craigslist.org/', 'https://muskegon.craigslist.org/', 'https://nmi.craigslist.org/', 'https://porthuron.craigslist.org/', 'https://saginaw.craigslist.org/', 'https://swmi.craigslist.org/', 'https://thumb.craigslist.org/', 'https://up.craigslist.org/'], 'Minnesota': ['https://bemidji.craigslist.org/', 'https://brainerd.craigslist.org/', 'https://duluth.craigslist.org/', 'https://mankato.craigslist.org/', 'https://minneapolis.craigslist.org/', 'https://rmn.craigslist.org/', 'https://marshall.craigslist.org/', 'https://stcloud.craigslist.org/'], 'Mississippi': ['https://gulfport.craigslist.org/', 'https://hattiesburg.craigslist.org/', 'https://jackson.craigslist.org/', 'https://meridian.craigslist.org/', 'https://northmiss.craigslist.org/', 'https://natchez.craigslist.org/'], 'Missouri': ['https://columbiamo.craigslist.org/', 'https://joplin.craigslist.org/', 'https://kansascity.craigslist.org/', 'https://kirksville.craigslist.org/', 'https://loz.craigslist.org/', 'https://semo.craigslist.org/', 'https://springfield.craigslist.org/', 'https://stjoseph.craigslist.org/', 'https://stlouis.craigslist.org/'], 'Montana': ['https://billings.craigslist.org/', 'https://bozeman.craigslist.org/', 'https://butte.craigslist.org/', 'https://greatfalls.craigslist.org/', 'https://helena.craigslist.org/', 'https://kalispell.craigslist.org/', 'https://missoula.craigslist.org/', 'https://montana.craigslist.org/'], 'Nebraska': ['https://grandisland.craigslist.org/', 'https://lincoln.craigslist.org/', 'https://northplatte.craigslist.org/', 'https://omaha.craigslist.org/', 'https://scottsbluff.craigslist.org/'], 'Nevada': ['https://elko.craigslist.org/', 'https://lasvegas.craigslist.org/', 'https://reno.craigslist.org/'], 'New Hampshire': ['https://nh.craigslist.org/'], 'New Jersey': ['https://cnj.craigslist.org/', 'https://jerseyshore.craigslist.org/', 'https://newjersey.craigslist.org/', 'https://southjersey.craigslist.org/'], 'New Mexico': ['https://albuquerque.craigslist.org/', 'https://clovis.craigslist.org/', 'https://farmington.craigslist.org/', 'https://lascruces.craigslist.org/', 'https://roswell.craigslist.org/', 'https://santafe.craigslist.org/'], 'New York': ['https://albany.craigslist.org/', 'https://binghamton.craigslist.org/', 'https://buffalo.craigslist.org/', 'https://catskills.craigslist.org/', 'https://chautauqua.craigslist.org/', 'https://elmira.craigslist.org/', 'https://fingerlakes.craigslist.org/', 'https://glensfalls.craigslist.org/', 'https://hudsonvalley.craigslist.org/', 'https://ithaca.craigslist.org/', 'https://longisland.craigslist.org/', 'https://newyork.craigslist.org/', 'https://oneonta.craigslist.org/', 'https://plattsburgh.craigslist.org/', 'https://potsdam.craigslist.org/', 'https://rochester.craigslist.org/', 'https://syracuse.craigslist.org/', 'https://twintiers.craigslist.org/', 'https://utica.craigslist.org/', 'https://watertown.craigslist.org/'], 'North Carolina': ['https://asheville.craigslist.org/', 'https://boone.craigslist.org/', 'https://charlotte.craigslist.org/', 'https://eastnc.craigslist.org/', 'https://fayetteville.craigslist.org/', 'https://greensboro.craigslist.org/', 'https://hickory.craigslist.org/', 'https://onslow.craigslist.org/', 'https://outerbanks.craigslist.org/', 'https://raleigh.craigslist.org/', 'https://wilmington.craigslist.org/', 'https://winstonsalem.craigslist.org/'], 'North Dakota': ['https://bismarck.craigslist.org/', 'https://fargo.craigslist.org/', 'https://grandforks.craigslist.org/', 'https://nd.craigslist.org/'], 'Ohio': ['https://akroncanton.craigslist.org/', 'https://ashtabula.craigslist.org/', 'https://athensohio.craigslist.org/', 'https://chillicothe.craigslist.org/', 'https://cincinnati.craigslist.org/', 'https://cleveland.craigslist.org/', 'https://columbus.craigslist.org/', 'https://dayton.craigslist.org/', 'https://limaohio.craigslist.org/', 'https://mansfield.craigslist.org/', 'https://sandusky.craigslist.org/', 'https://toledo.craigslist.org/', 'https://tuscarawas.craigslist.org/', 'https://youngstown.craigslist.org/', 'https://zanesville.craigslist.org/'], 'Oklahoma': ['https://lawton.craigslist.org/', 'https://enid.craigslist.org/', 'https://oklahomacity.craigslist.org/', 'https://stillwater.craigslist.org/', 'https://tulsa.craigslist.org/'], 'Oregon': ['https://bend.craigslist.org/', 'https://corvallis.craigslist.org/', 'https://eastoregon.craigslist.org/', 'https://eugene.craigslist.org/', 'https://klamath.craigslist.org/', 'https://medford.craigslist.org/', 'https://oregoncoast.craigslist.org/', 'https://portland.craigslist.org/', 'https://roseburg.craigslist.org/', 'https://salem.craigslist.org/'], 'Pennsylvania': ['https://altoona.craigslist.org/', 'https://chambersburg.craigslist.org/', 'https://erie.craigslist.org/', 'https://harrisburg.craigslist.org/', 'https://lancaster.craigslist.org/', 'https://allentown.craigslist.org/', 'https://meadville.craigslist.org/', 'https://philadelphia.craigslist.org/', 'https://pittsburgh.craigslist.org/', 'https://poconos.craigslist.org/', 'https://reading.craigslist.org/', 'https://scranton.craigslist.org/', 'https://pennstate.craigslist.org/', 'https://williamsport.craigslist.org/', 'https://york.craigslist.org/'], 'Rhode Island': ['https://providence.craigslist.org/'], 'South Carolina': ['https://charleston.craigslist.org/', 'https://columbia.craigslist.org/', 'https://florencesc.craigslist.org/', 'https://greenville.craigslist.org/', 'https://hiltonhead.craigslist.org/', 'https://myrtlebeach.craigslist.org/'], 'South Dakota': ['https://nesd.craigslist.org/', 'https://csd.craigslist.org/', 'https://rapidcity.craigslist.org/', 'https://siouxfalls.craigslist.org/', 'https://sd.craigslist.org/'], 'Tennessee': ['https://chattanooga.craigslist.org/', 'https://clarksville.craigslist.org/', 'https://cookeville.craigslist.org/', 'https://jacksontn.craigslist.org/', 'https://knoxville.craigslist.org/', 'https://memphis.craigslist.org/', 'https://nashville.craigslist.org/', 'https://tricities.craigslist.org/'], 'Texas': ['https://abilene.craigslist.org/', 'https://amarillo.craigslist.org/', 'https://austin.craigslist.org/', 'https://beaumont.craigslist.org/', 'https://brownsville.craigslist.org/', 'https://collegestation.craigslist.org/', 'https://corpuschristi.craigslist.org/', 'https://dallas.craigslist.org/', 'https://nacogdoches.craigslist.org/', 'https://delrio.craigslist.org/', 'https://elpaso.craigslist.org/', 'https://galveston.craigslist.org/', 'https://houston.craigslist.org/', 'https://killeen.craigslist.org/', 'https://laredo.craigslist.org/', 'https://lubbock.craigslist.org/', 'https://mcallen.craigslist.org/', 'https://odessa.craigslist.org/', 'https://sanangelo.craigslist.org/', 'https://sanantonio.craigslist.org/', 'https://sanmarcos.craigslist.org/', 'https://bigbend.craigslist.org/', 'https://texoma.craigslist.org/', 'https://easttexas.craigslist.org/', 'https://victoriatx.craigslist.org/', 'https://waco.craigslist.org/', 'https://wichitafalls.craigslist.org/'], 'Utah': ['https://logan.craigslist.org/', 'https://ogden.craigslist.org/', 'https://provo.craigslist.org/', 'https://saltlakecity.craigslist.org/', 'https://stgeorge.craigslist.org/'], 'Vermont': ['https://vermont.craigslist.org/'], 'Virginia': ['https://charlottesville.craigslist.org/', 'https://danville.craigslist.org/', 'https://fredericksburg.craigslist.org/', 'https://norfolk.craigslist.org/', 'https://harrisonburg.craigslist.org/', 'https://lynchburg.craigslist.org/', 'https://blacksburg.craigslist.org/', 'https://richmond.craigslist.org/', 'https://roanoke.craigslist.org/', 'https://swva.craigslist.org/', 'https://winchester.craigslist.org/'], 'Washington': ['https://bellingham.craigslist.org/', 'https://kpr.craigslist.org/', 'https://moseslake.craigslist.org/', 'https://olympic.craigslist.org/', 'https://pullman.craigslist.org/', 'https://seattle.craigslist.org/', 'https://skagit.craigslist.org/', 'https://spokane.craigslist.org/', 'https://wenatchee.craigslist.org/', 'https://yakima.craigslist.org/'], 'West Virginia': ['https://charlestonwv.craigslist.org/', 'https://martinsburg.craigslist.org/', 'https://huntington.craigslist.org/', 'https://morgantown.craigslist.org/', 'https://wheeling.craigslist.org/', 'https://parkersburg.craigslist.org/', 'https://swv.craigslist.org/', 'https://wv.craigslist.org/'], 'Wisconsin': ['https://appleton.craigslist.org/', 'https://eauclaire.craigslist.org/', 'https://greenbay.craigslist.org/', 'https://janesville.craigslist.org/', 'https://racine.craigslist.org/', 'https://lacrosse.craigslist.org/', 'https://madison.craigslist.org/', 'https://milwaukee.craigslist.org/', 'https://northernwi.craigslist.org/', 'https://sheboygan.craigslist.org/', 'https://wausau.craigslist.org/'], 'Wyoming': ['https://wyoming.craigslist.org/'], 'Territories': ['https://micronesia.craigslist.org/', 'https://puertorico.craigslist.org/', 'https://virgin.craigslist.org/']}
state_city_zip_mapping = [{'state': 'Alabama', 'city': 'Huntsville', 'type': 'range', 'zips': ['35801', '35816']}, {'state': 'Alaska', 'city': 'Anchorage', 'type': 'range', 'zips': ['99501', '99524']}, {'state': 'Arizona', 'city': 'Phoenix', 'type': 'range', 'zips': ['85001', '85055']}, {'state': 'Arkansas', 'city': 'Little Rock', 'type': 'range', 'zips': ['72201', '72217']}, {'state': 'California', 'city': 'Sacramento', 'type': 'range', 'zips': ['94203', '94209']}, {'state': 'California', 'city': 'Los Angeles', 'type': 'range', 'zips': ['90001', '90089']}, {'state': 'California', 'city': 'Beverly Hills', 'type': 'range', 'zips': ['90209', '90213']}, {'state': 'Colorado', 'city': 'Denver', 'type': 'range', 'zips': ['80201', '80239']}, {'state': 'Conneticut', 'city': 'Hartford', 'type': 'range', 'zips': ['06101', '06112']}, {'state': 'Deleware', 'city': 'Dover', 'type': 'range', 'zips': ['19901', '19905']}, {'state': 'District', 'city': 'Washington', 'type': 'range', 'zips': ['20001', '20020']}, {'state': 'Florida', 'city': 'Pensacola', 'type': 'range', 'zips': ['32501', '32509']}, {'state': 'Florida', 'city': 'Miami', 'type': 'range', 'zips': ['33124', '33190']}, {'state': 'Florida', 'city': 'Orlando', 'type': 'range', 'zips': ['32801', '32837']}, {'state': 'Georgia', 'city': 'Atlanta', 'type': 'range', 'zips': ['30301', '30381']}, {'state': 'Hawaii', 'city': 'Honolulu', 'type': 'range', 'zips': ['96801', '96830']}, {'state': 'Idaho', 'city': 'Montpelier', 'type': 'list', 'zips': ['83254']}, {'state': 'Illinois', 'city': 'Chicago', 'type': 'range', 'zips': ['60601', '60641']}, {'state': 'Illinois', 'city': 'Springfield', 'type': 'range', 'zips': ['62701', '62709']}, {'state': 'Indiana', 'city': 'Indianapolis', 'type': 'range', 'zips': ['46201', '46209']}, {'state': 'Iowa', 'city': 'Davenport', 'type': 'range', 'zips': ['52801', '52809']}, {'state': 'Iowa', 'city': 'Des Moines', 'type': 'range', 'zips': ['50301', '50323']}, {'state': 'Kansas', 'city': 'Wichita', 'type': 'range', 'zips': ['67201', '67221']}, {'state': 'Kentucky', 'city': 'Hazard', 'type': 'list', 'zips': ['41701', '41702']}, {'state': 'Lousiana', 'city': 'New Orleans', 'type': 'range', 'zips': ['70112', '70119']}, {'state': 'Maine', 'city': 'Freeport', 'type': 'range', 'zips': ['04032', '04034']}, {'state': 'Maryland', 'city': 'Baltimore', 'type': 'range', 'zips': ['21201', '21237']}, {'state': 'Massachusetts', 'city': 'Boston', 'type': 'range', 'zips': ['02101', '02137']}, {'state': 'Michigan', 'city': 'Coldwater', 'type': 'list', 'zips': ['49036']}, {'state': 'Michigan', 'city': 'Gaylord', 'type': 'list', 'zips': ['49734', '49735']}, {'state': 'Minnesota', 'city': 'Duluth', 'type': 'range', 'zips': ['55801', '55808']}, {'state': 'Mississippo', 'city': 'Biloxi', 'type': 'range', 'zips': ['39530', '39535']}, {'state': 'Missouri', 'city': 'St. Louis', 'type': 'range', 'zips': ['63101', '63141']}, {'state': 'Montana', 'city': 'Laurel', 'type': 'list', 'zips': ['59044']}, {'state': 'Nebraska', 'city': 'Hastings', 'type': 'list', 'zips': ['68901', '68902']}, {'state': 'Nevada', 'city': 'Reno', 'type': 'range', 'zips': ['89501', '89513']}, {'state': 'New', 'city': 'Ashland', 'type': 'list', 'zips': ['3217']}, {'state': 'New', 'city': 'Livingston', 'type': 'list', 'zips': ['7039']}, {'state': 'New', 'city': 'Santa Fe', 'type': 'range', 'zips': ['87500', '87506']}, {'state': 'New', 'city': 'New York', 'type': 'range', 'zips': ['10001', '10048']}, {'state': 'North', 'city': 'Oxford', 'type': 'list', 'zips': ['27565']}, {'state': 'North', 'city': 'Walhalla', 'type': 'list', 'zips': ['58282']}, {'state': 'Ohio', 'city': 'Cleveland', 'type': 'range', 'zips': ['44101', '44179']}, {'state': 'Oklahoma', 'city': 'Tulsa', 'type': 'range', 'zips': ['74101', '74110']}, {'state': 'Oregon', 'city': 'Portland', 'type': 'range', 'zips': ['97201', '97225']}, {'state': 'Pennsylvania', 'city': 'Pittsburgh', 'type': 'range', 'zips': ['15201', '15244']}, {'state': 'Rhode', 'city': 'Newport', 'type': 'list', 'zips': ['02840', '02841']}, {'state': 'South', 'city': 'Camden', 'type': 'list', 'zips': ['29020']}, {'state': 'South', 'city': 'Aberdeen', 'type': 'list', 'zips': ['57401', '57402']}, {'state': 'Tennessee', 'city': 'Nashville', 'type': 'range', 'zips': ['37201', '37222']}, {'state': 'Texas', 'city': 'Austin', 'type': 'range', 'zips': ['78701', '78705']}, {'state': 'Utah', 'city': 'Logan', 'type': 'range', 'zips': ['84321', '84323']}, {'state': 'Vermont', 'city': 'Killington', 'type': 'list', 'zips': ['5751']}, {'state': 'Virginia', 'city': 'Altavista', 'type': 'list', 'zips': ['24517']}, {'state': 'Washington', 'city': 'Bellevue', 'type': 'range', 'zips': ['98004', '98009']}, {'state': 'West', 'city': 'Beaver', 'type': 'list', 'zips': ['25813']}, {'state': 'Wisconsin', 'city': 'Milwaukee', 'type': 'range', 'zips': ['53201', '53228']}, {'state': 'Wyoming', 'city': 'Pinedale', 'type': 'list', 'zips': ['82941']}]



/*
-1 = these were set by looking up the zip for each city name (these are most likely correct)
-2 = i forgot what -2 was..
-3 = was when i queried for all posts on a page and did a zip look up based on location of the page
*/

db.loadServerScripts();

db.system.js.save(
   {
        _id: "addUpdateZips",
        value : function addUpdateZips(zs, clSubDomain){
            throw 'Make sure you change the distance value';
            let updated = db.realestate.clzips.updateMany({zip: {$in:zs}, clSubDomain:{$exists:false}}, {$set:{disabled:false, clSubDomain: clSubDomain, distance: -3}});
            print("Update result: ", JSON.stringify(updated))
            let existing_zips = db.realestate.clzips.find({zip:{$in:zs}}, {zip:1})
            let existing = new Set();
            existing_zips.forEach(z => {
                existing.add(z.zip)
            });
            let not_exist = zs.filter(zip => !existing.has(zip));
            not_exist.forEach(zip => {
                let obj = {
                    zip: zip,
                    processingRequestTime: 1,
                    lastCompletedProcess: 1,
                    clSubDomain: clSubDomain,
                    distance: -3,
                    disabled: false
                }
                db.realestate.clzips.save(obj);
            });
            print('Created: ', not_exist.length)
        }
   }
)

db.system.js.save(
   {
        _id: "mean",
        value : function mean(numbers) {
            var total = 0, i;
            for (i = 0; i < numbers.length; i += 1) {
                total += numbers[i];
            }
            return Math.floor(((total / numbers.length)*100))/100;
        }
   }
)

db.system.js.save(
   {
        _id: "median",
        value : function median(numbers) {
            // median of [3, 5, 4, 4, 1, 1, 2, 3] = 3
            var median = 0, numsLen = numbers.length;
            numbers.sort();

            if (
                numsLen % 2 === 0 // is even
            ) {
                // average of two middle numbers
                median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
            } else { // is odd
                // middle number only
                median = numbers[(numsLen - 1) / 2];
            }

            return median;
        }
   }
)

db.system.js.save(
   {
        _id: "range",
        value : function range(numbers) {
        numbers.sort((a,b)=> b - a);
        return [numbers[numbers.length - 1], numbers[0]];
    }
   }
)


function statsForZip(zip, numBeds, numBaths, startDate, endDate){
    let q = {zip: String(zip), removedTime: {$exists:true}}
    if(startDate){
        let parts = startDate.split('/');
        start = new Date(parts[2], parts[0]-1, parts[1]).getTime()/1000;
        q.postDate = {$gte: start}
    }
    if(endDate){
        let parts = endDate.split('/');
        end = new Date(parts[2], parts[0]-1, parts[1]).getTime()/1000;
        q.postDate = {$lte: end}
    }
    if(typeof numBeds != 'undefined'){
        q.numBedrooms = numBeds;
    }
    if(typeof numBaths != 'undefined'){
        q.numBaths = numBaths;
    }
    print(`query = ${JSON.stringify(q)}\n`)
    let posts = db.realestate.clposts.find(q, {price:1});
    let prices = []
    posts.forEach(post => {
        prices.push(post.price);
    });

    let meen = mean(prices);
    let meedian = median(prices);
    let renge = range(prices);
    print(`${zip} - mean: ${meen} median: ${meedian} range: ${renge}. # data points: ${prices.length}`)
}

db.system.js.save(
   {
        _id: "priceStatsForZip",
        value : function priceStatsForZip(zip, numBeds, numBaths, startDate, endDate){
            let q = {zip: String(zip), removedTime: {$exists:true}}
            if(startDate){
                let parts = startDate.split('/');
                start = new Date(parts[2], parts[0]-1, parts[1]).getTime()/1000;
                q.postDate = {$gte: start}
            }
            if(endDate){
                let parts = endDate.split('/');
                end = new Date(parts[2], parts[0]-1, parts[1]).getTime()/1000;
                q.postDate = {$lte: end}
            }
            if(typeof numBeds != 'undefined'){
                q.numBedrooms = numBeds;
            }
            if(typeof numBaths != 'undefined'){
                q.numBaths = numBaths;
            }
            print(`query = ${JSON.stringify(q)}\n`)
            let posts = db.realestate.clposts.find(q, {price:1});
            let prices = []
            posts.forEach(post => {
                prices.push(post.price);
            });

            let meen = mean(prices);
            let meedian = median(prices);
            let renge = range(prices);
            print(`${zip} - mean: ${meen} median: ${meedian} range: ${renge}. # data points: ${prices.length}`)
        }
   }
)


function getAllZipsWithNoData(){
    let zips = db.realestate.clzips.find({disabled:false});
    let noPosts = []
    zips.forEach(zip => {
        if(db.realestate.clposts.count({zip: zip.zip}) == 0){
            noPosts.push(zip.zip)
        }
    })
    print(noPosts)
    return noPosts
}